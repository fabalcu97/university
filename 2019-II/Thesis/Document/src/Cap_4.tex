\chapter{Propuesta}

El presente trabajo busca mejorar el reconocimiento de las emociones que un usuario percibe a partir de su actividad cerebral. Al haber diferentes etapas en la extracción de la actividad cerebral, la propuesta se enfoca en realizar comparaciones en diferentes etapas, la primera consiste en métodos para extraer y seleccionar características, la segunda es una comparación entre algoritmos de clasificación para obtener el resultado final.

A continuación se describirán las tareas a realizar, que comprenden desde la extracción de actividad cerebral hasta la clasificación. Inicialmente se posicionan los electrodos alrededor de la cabeza, haciendo uso del estándar 10-20 (ver Figura \ref{fig:10_20}), con esto se comienza a \textbf{registrar} la actividad eléctrica del cerebro, siendo este registro la entrada de la etapa de pre-procesamiento. El pre-procesamiento consiste en \textbf{filtrar} la información que no es útil para las siguientes fases, los errores de observación. Además, se puede reducir el rango de frecuencias para dejar solo frecuencias óptimas para el reconocimiento, que comprenden un rango entre 4Hz y 45Hz \cite{Liu2010}. De las señales pre-procesadas se procede a \textbf{extraer} las características adecuadas, estas suelen ser de carácter estadístico como la media de las frecuencias, o descriptivo como la entropía o energía de las ondas registradas. Finalmente se puede \textbf{procesar} las características mediante algoritmos de clasificación, como por ejemplo \ac{SVM}, la salida de la clasificación se puede trasladar al modelo bi-dimensional (ver Figura \ref{fig:arousal_valence}) teniendo así el resultado de la emoción percibida por el usuario.

\section{Extracción de la Actividad Cerebral}
La etapa de extracción consiste en posicionar los electrodos sobre la cabeza del individuo (ver Figura \ref{fig:10_20}), una vez hecho esto, los electrodos leen la actividad cerebral y comienza el registro de las ondas recibidas que representan el voltage a través del tiempo. Cada canal o electrodo permite registrar una onda y en el presente trabajo, se toma en consideración los canales F3-F4, F7-F8, FC1-FC2, FC5-FC6 y FP1-FP2, esto se posicionan en la parte frontal de la cabeza pues de acuerdo a Coan et al. \cite{Coan2001} tienen mayor relación con la reacción emocional de las personas. El registro se da con una frecuencia de 128Hz y solo se almacena el valor del voltage en $\mu V$, son esto datos los que se usan en el pre-procesamiento.

% TODO: Traducir al español.
\begin{figure}[h]
  \centering
  \includegraphics[width=10cm]{assets/eeg_sample.png}
  \caption{Muestra de señales extraídas de los canales FP1, FP2, Cz, O1 y O2 \cite{Singh2017}}
  \label{fig:eeg_sample}
\end{figure}

\section{Pre-Procesamiento}
Hay diversos factores que influyen en la adquisición de señales eléctricas, se tiene influencia de factores externos como fuentes de corriente. El cerebro es el centro de control de todo el cuerpo, es gracias a él que respiramos, parpadeamos o movemos músculos, sin embargo estas actividades son realizadas de manera involuntaria y por ello se registran también en los registros de la etapa anterior. Para eliminar los errores de observación mencionados se debe aplicar filtros que eliminen frecuencias no deseadas. Se realiza una reducción en las frecuencias, pues sabemos que la información adecuada para las tareas siguientes se halla entre los rangos de 4Hz y 45Hz, con eso se eliminan ruidos externos pues suelen ser de mayor frecuencia \cite{Mohammadi2016}, el ruido generado por los latidos del corazón también es eliminado pues se encuentra usualmente en frecuencias cercanas a 1Hz \cite{Tandle2015}. Finalmente para eliminar el ruido generado por el parpadeo, se realizan una descomposición de la señal mediante wavelets, se detectan los tiempos de parpadeo y su reflejo en la señal (ver Figura \ref{fig:ocular_remotion}), se elimina el pico y se recompone la señal sin dicho ruido \cite{Xiaobai2016}.

% TODO: Traducir al español.
\begin{figure}[h]
  \centering
  \includegraphics[width=10cm]{assets/ocular_remotion.png}
  \caption{Comparación entre una señal original (azul) y el resultado de haber eliminado los artefactos oculares (rojo) \cite{Xiaobai2016}.}
  \label{fig:ocular_remotion}
\end{figure}

\section{Extracción y Selección de Características}

\subsection{Extracción de Características}

La extracción de características es la primera etapa de un proceso que tiene como objetivo reducir la dimensionalidad del conjunto de datos inicial. Los vectores contienen datos característicos de las señales registradas, estos datos ayudan a la clasificación, sin embargo suelen ser abundantes, causando un aumento en la complejidad computacional. Al extraer características adecuadas, se reduce la dimensionalidad, reduciendo de igual manera el costo computacional y manteniendo una representación fiel del vector inicial.

Para realizar la extracción de características existen diversos métodos y variables. Dependiendo de la naturaleza de la señal, se recomienda la transformación del dominio. Para el caso de las señales \ac{EEG}, al ser de naturaleza no estacionaria, se recomienda usar \ac{DWT}, pues esta diseñada para este tipo de señales, sin embargo puede resultar contraproducente para grandes cantidades de datos, razón por la que se puede considerar ventanas de tiempo y así reducir el costo computacional \cite{Supratak2016}.

\subsubsection{Características a Extraer}
De la señal en bruto, se puede extraer características como la media (ver Ecuación \ref{eqn:mean}), la desviación estándar (ver Ecuación \ref{eqn:deviation}), la media de los valores absolutos de la primera diferencia (ver Ecuación \ref{eqn:first_difference}), la media de los valores absolutos de la primera diferencia de la señal normalizada (ver Ecuación \ref{eqn:normalized_first_difference}), la media de los valores absolutos de la segunda diferencia (ver Ecuación \ref{eqn:second_difference}) y Media de los valores absolutos de la segunda diferencia de la señal normalizada (ver Ecuación \ref{eqn:normalized_second_difference}) \cite{Zubair2017}.

Cuando a la señal se le aplica \ac{DWT}, pasa al dominio de la frecuencia permitiendo extraer más características como la energía (ver Ecuación \ref{eqn:energy}), la entropía (ver Ecuación \ref{eqn:entropy}), o \cite{Murugappan2010, Murugappan2008, Zubair2017}:
\begin{itemize}
  \item \ac{REE}
        \begin{equation}
          \label{eqn:ree}
          REE_{gamma} = \frac{E_{\gamma}}{E_{total}}
        \end{equation}
  \item \ac{LREE}
        \begin{equation}
          \label{eqn:lree}
          LREE_{gamma} = \log_{10}(\frac{E_{\gamma}}{E_{total}})
        \end{equation}
  \item \ac{ALREE}
        \begin{equation}
          \label{eqn:alree}
          ALREE_{gamma} = |log_{10}(\frac{E_{\gamma}}{E_{total}})|
        \end{equation}
\end{itemize}

\subsection{Selección de características}
La extracción de características permite obtener datos que describen las señales, sin embargo la cantidad de dimensiones que componen los vectores afecta el rendimiento de la clasificación, razón por la cual se analiza dichos vectores con el fin de reducir los datos menos relevantes.

\subsubsection{Mínima Redundancia, Máxima Relevancia}
Propuesto inicialmente por Ding et. al. \cite{CHRIS2005}, donde se buscó realizar la selección de genes (características) adecuados para determinar fenotipos o características visuales de los organismos.

El algoritmo busca obtener la máxima dependencia entre un conjunto de características $X$ y una clase $C$, haciendo uso de la \ac{MI} (ver Ecuación \ref{eqn:mutual_information}), donde $p(a)$ y $p(b)$ es la probabilidad marginal, mientras que $p(a, b)$ es la probabilidad conjunta.

\begin{equation}
  \label{eqn:mutual_information}
  I(A, B) = \sum_{b\in B}{\sum_{a\in A}p(a, b)log(\frac{p(a, b)}{p(a)p(b)})}
\end{equation}

La máxima relevancia se define mediante la Ecuación \ref{eqn:max_relevance}, donde $X$ es el vector de características de entrada, $|X|$ es la cantidad de características o longitud del vector y $C$ es la clase objetivo. 

\begin{equation}
  \label{eqn:max_relevance}
  max M(X, C); M(X, C) = \frac{1}{|X|}\sum_{x_{i}\in X}I(x_i, C)
\end{equation}

Obtener la máxima relevancia puede resultar en tener una gran cantidad de elementos redundantes. Para reducir la redundancia se aplica el criterio de mínima redundancia, definido por la Ecuación \ref{eqn:min_relevance}.

\begin{equation}
  \label{eqn:min_relevance}
  min m(X); m(X) = \frac{1}{|X|}\sum_{x_{i}, x_{j}\in X}I(x_i, x_j)
\end{equation}

Finalmente, el resultado de aplicar las operaciones previas se determina por criterio, \ac{MIQ} (ver Ecuación \ref{eqn:miq}) o \ac{MID} (ver Ecuación \ref{eqn:mid}).

\begin{equation}
  \label{eqn:miq}
  max(M / m)
\end{equation}

\begin{equation}
  \label{eqn:mid}
  max(M - m)
\end{equation}

\subsubsection{\textit{Codificador Automåtico Esparcido}}

Los codificadores automáticos son redes neuronales artificiales no supervisadas. Buscan comprimir información reduciendo ruido o seleccionando información relevante, una vez determinado el conjunto de datos comprimido, el codificador automático busca reconstruir la información para llegar a una representación tan fiel a la original como sea posible (ver Figura \ref{fig:sae_sample}). Consta de una capa de entrada, una capa intermedia y una capa de salida (ver Figura \ref{fig:sae_structure}).

% TODO: Traducir al español.
\begin{figure}[h]
  \centering
  \includegraphics[width=10cm]{assets/sae_sample.png}
  \caption{Ejemplo de cómo funciona un codificador automático \cite{Badr2019}.}
  \label{fig:sae_sample}
\end{figure}

En el codificador automático esparcido, se consideran las señales \ac{EEG} en bruto para la entrada de la red neuronal, $X = \{x_{1}, x_{2}, ..., x_{n}\}$. La capa de salida retorna la señal reconstruida y similar a la señal de entrada $\hat{X} = \{\hat{x_{1}}, \hat{x_{1}}, ..., \hat{x_{n}}\}$. $\hat{X}$ se obtiene mediante la función de activación (ver Ecuación \ref{eqn:sae_activation}).

\begin{equation}
  \label{eqn:sae_activation}
  h(x) = f(W_{x} + b)
\end{equation}

\begin{equation}
  \label{eqn:sae_sigmoid}
  f(x) = \frac{1}{(1 + \exp(-z))}
\end{equation}

Donde $W$ son los pesos y $b$ es el sesgo de la función, obtenidos de la capa intermedia.

% TODO: Traducir al español.
\begin{figure}[h]
  \centering
  \includegraphics[width=10cm]{assets/sae_structure.png}
  \caption{Arquitectura del codificador automático esparcido \cite{Yan2016}.}
  \label{fig:sae_structure}
\end{figure}

El codificador automático esparcido es entrenado haciendo uso de propagación hacia atrás con el fin de reducir el error de reconstrucción de la señal.

\begin{equation}
  \label{eqn:cost_function}
  J_{sparse}(W, b, X, \hat{X}) = \frac{1}{2}||h_{W, b}(X) - \hat{X}||^{2}
\end{equation}

\begin{equation}
  \label{eqn:overall_cost_function}
  J_{sparse} (W, b) = [\frac{1}{M}\sum_{i=1}^{M}\frac{1}{2}||h_{W, b}(X) - \hat{X}||^{2}] + \frac{\lambda}{2}\sum_{i=1}^{N-1}\sum_{j=1}^{N}(W_{ij})^{2}
\end{equation}

En la Ecuación \ref{eqn:overall_cost_function}, el primer término indica un promedio de los errores cuadráticos entre la señal original y la señal reconstruída. El segundo término es un factor de regularización, busca reducir la magnitud de los pesos para prevenir errores de sobre-entrenamiento.

En el inicio del entrenamiento, se definen los pesos ($W$) y el sesgo ($b$) como valores aleatorios cercanos a cero. En cada iteración se actualizan los pesos y el bias de la siguiente manera:

\begin{equation}
  \label{eqn:udpate_w}
  W_{ij} = W_{ij} - \alpha \frac{\delta}{\delta{W_{ij}}}J_{sparse}(W, b)
\end{equation}

\begin{equation}
  \label{eqn:udpate_b}
  b_{i} = b_{i} - \alpha \frac{\delta}{\delta b_{i}}J_{sparse}(W, b)
\end{equation}

Donde $\alpha$ es la taza de aprendizaje, y $\frac{\delta}{\delta b_{i}}J_{sparse}(W, b)$ con $\frac{\delta}{\delta{W_{ij}}}J_{sparse}(W, b)$ son las derivadas parciales de la función de costo, definidas como:

\begin{equation}
  \label{eqn:derivate_w}
  \frac{\delta}{\delta{W_{ij}}}J_{sparse}(W, b) = [\frac{1}{M}\sum_{i=1}^{M}\frac{\delta}{\delta{W_{ij}}}J_{sparse}(W, b, X, \hat{X})] + \lambda W_{ij}
\end{equation}

\begin{equation}
  \label{eqn:derivate_b}
  \frac{\delta}{\delta b_{i}}J_{sparse}(W, b) = [\frac{1}{M}\sum_{i=1}^{M}\frac{\delta}{\delta{b_{i}}}J_{sparse}(W, b, X, \hat{X})]
\end{equation}

Una vez que el modelo es entrenado, se elimina la capa de salida, haciendo que la capa intermedia entregue las características relevantes a la siguiente etapa, la clasificación.

\section{Clasificación}

Con las características elegidas en las etapa anteriores, se lleva a cabo la clasificación. En el presente trabajo se realizará la clasificación con \ac{SVM} y \ac{KNN}, Para el \ac{SVM} se empleará un núcleo polinomial y un núcleo de \ac{RBF}. Además, se realizará la clasificación en dos dimensiones, la primera es valencia, y la segunda excitación, para finalmente realizar un mapeo de los resultados al modelo bi-dimensional y obtener la emoción resultante.