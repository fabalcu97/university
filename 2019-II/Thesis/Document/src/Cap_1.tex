\chapter{Introducción}

\section{Motivación y Contexto}

El reconocimiento de emociones se vuelve una parte importante de la interacción entre humanos y computadores pues tiene un efecto directo sobre el trabajo de una persona o su actuar cotidiano. Estudios han demostrado que un manejo adecuado del estado anímico de una persona permite un desempeño óptimo en sus tareas cotidianas\cite{Judge1998, Pervez2010}, así como en deportes\cite{Lazarus2000} y el trabajo\cite{Pervez2010}.

Actualmente, hay múltiples métodos para lograr dicho reconocimiento, existe el análisis de expresiones faciales, del habla, de textos o análisis de la actividad cerebral, siendo este último un buen complemento para el resto de métodos como es el caso de fusión entre expresiones faciales y \ac{EEG}\cite{Koelstra2013}.

El reconocimiento de emociones por interfaces cerebrales consta de cuatro etapas, inicialmente es la extracción de la actividad, llevada a cabo por medio de electrodos, extraen la actividad electromagnética del cerebro para luego realizar un pre-procesamiento que elimina ruidos en la señal. Acto seguido se da la extracción de características y finalmente se realiza el reconocimiento de las emociones por medio de clasificadores entrenados para dicho propósito.

A pesar de tener un flujo definido hay múltiples factores que afectan el resultado de la clasificación. Respecto a la extracción, para extraer la información la actividad cerebral, \ac{EEG} es la más usada por el costo bajo y el bajo riesgo para el usuario, además de tener una buena la resolución temporal. En cuanto al posicionamiento en el cerebro, un estudio sobre la relación entre las emociones y la actividad cerebral\cite{Coan2001} concluye que hay una relación con los lóbulos frontales del cerebro, siendo el lóbulo frontal izquierdo útil para las emociones positivas y el derecho para las emociones negativas. En cuanto a la clasificación, se puede considerar dos esquemas, el primero es una clasificación de manera discreta, seleccionando una cantidad fija de emociones. El segundo esquema es un modelo bi-dimensional basado en la excitación y la valencia, este modelo pone a disposición un mayor número de emociones pues considera dos dimensiones de intensidad, por ejemplo una persona excitada al máximo y algo disgustada (valencia) puede representar nerviosismo, ver Figura \ref{fig:arousal_valence}.

\begin{figure}[h]
\center
\includegraphics[scale = 0.6]{assets/arousal_valence.png}
\caption{Modelo bi-dimensional basado en \textit{arousal} y \textit{valence}}
\label{fig:arousal_valence}\
\end{figure}

\section{Planteamiento del Problema}

Las emociones tienen un efecto directo sobre el actuar de las personas, lo que implica un efecto en el rendimiento de las actividades que buscamos realizar. Actualmente los dispositivos electrónicos también se insertan en lo cotidiano; es común ver que las personas atienden sus celulares o computadoras constantemente a medida que realizan sus actividades. Modificar los entornos de interacción de los sistemas computacionales al gusto del usuario o, también, en función de un estímulo exterior es una tarea realizable hoy en día. Si el detonante de una modificación en la interacción es el estado emocional que una persona percibe en un momento dado, se puede brindar una experiencia personalizada al usuario, haciendo que su labor sea más agradable y vaya de acuerdo a su sentir.

Para poder suplir a los sistemas de interacción modificables con información emocional del usuario, es necesario hacer un reconocimiento de las mismas, mediante un proceso de extracción y clasificación que debe conseguir la mayor precisión posible. Una parte importante de la clasificación es la extracción de características, usualmente las señales comparten cierta información como su amplitud, o energía, dicha información ayuda a reconocer patrones o clases en las señales, sin embargo el problema en las señales \ac{EEG} yace en la estructura, pues la información cerebral de las personas es distinta. Zeynali et. al \cite{Zeynali2019} indican que estas señales contienen patrones únicos en la persona, por lo que pueden considerarse para aplicaciones en biometría. Entonces, si la información cerebral es tan distinta entre las personas, es necesario realizar un reconocimiento adecuado de la información representativa para que la clasificación sea tan precisa como sea posible.

\section{Objetivo}

Implementar un sistema de selección de características de señales cerebrales que sea óptimo y  alimentar a un sistema de clasificación de emociones usando redes neuronales.

\subsection{Objetivos Específicos}

\begin{itemize}
  \item Analizar las técnicas del estado del arte con el fin de extraer componentes relevantes y de utilidad en el presente trabajo.
  \item Implementar \ac{mRMR} y \ac{SAE} para la selección de características. 
  \item Implementar \ac{SVM} y \ac{KNN} para realizar la clasificación.
  \item Realizar pruebas con los conjuntos de datos \ac{DEAP} y MAHNOB-HCI.
  \item Realizar comparaciones de precisión.
\end{itemize}

\section{Organización del plan de Tesis}
El resto del plan de tesis esta estructurado de la siguiente manera.

El capítulo 2 explica los fundamentos de las redes neuronales a implementar, así como las etapas previas a la clasificación en la extracción de señales \ac{EEG}.

El capítulo 3 consiste en el estado del arte de las técnicas para el reconocimiento de emociones.

El capítulo 4 describe el funcionamiento del algoritmo propuesto y cada una de sus partes.

El capítulo 5 consiste en las pruebas y resultados obtenidos de las comparaciones.

Finalmente, el capítulo 6 presentará las conclusiones del presente trabajo.

\section{Cronograma}

A continuación se muestra el cronograma de avance que corresponde a las presentaciones semanales del curso:
\begin{enumerate}
  \item Implementar \ac{mRMR} y \ac{SAE} \textbf{(35\%)}:
    \begin{itemize}
      \item Presentación de las versiones encontradas de los algoritmos de \ac{mRMR}. Se ejecutarán pruebas sobre cada uno variando la cantidad de características a escoger. - \textbf{Semana de 26/10}
      \item Presentación de \ac{SAE}. Se realizarán pruebas sobre la red neuronal con el fin de variar las iteraciones y la cantidad de neuronas en la capa oculta. - \textbf{Semana de 9/11}
      \item Implementación de \ac{SVM} y \ac{KNN}. Se hará la clasificación de las señales con ambos algoritmos, buscando determinar el desempeño de \ac{SAE} y simular el desempeño registrado previamente con \ac{mRMR}.  - \textbf{Semana de 16/11}
      \item Presentación de resultados. - \textbf{Semana de 23/11}
    \end{itemize}
  \item Implementar una red neuronal profunda Kohonen con propagación circular reversa para la clasificación de emociones.
  \item Hacer el entrenamiento y pruebas de la red neuronal con los conjuntos de datos \ac{DEAP} y MAHNOB-HCI.
\end{enumerate}
