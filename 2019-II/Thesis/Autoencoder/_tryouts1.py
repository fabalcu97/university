# lstm autoencoder reconstruct and predict sequence
from keras import Sequential, regularizers, Input
from numpy import array
from keras.models import Model
from keras.layers import Dense, LSTM, RepeatVector, TimeDistributed
import matplotlib.pyplot as plt
import numpy as np

from DataProvider import DataProvider


def plot(original, reconstructed, encoded):
    n = len(original)  # how many digits we will display
    rows = 3
    plt.figure(figsize=(50, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(rows, n, i + 1)
        plt.plot(original[i])
        plt.gray()
        ax.set_ylim([-1, 1])

        # display reconstruction
        ax = plt.subplot(rows, n, i + 1 + n)
        plt.plot(reconstructed[i])
        plt.cool()
        ax.set_ylim([-1, 1])

        ax = plt.subplot(rows, n, i + 1 + 2*n)
        plt.plot(encoded[i])
        plt.copper()
        ax.set_ylim([-1, 1])

    plt.show()


def get_dataset():
    data = DataProvider(file_number=1)
    # For each experiment, extract one channel

    x_train = list()
    x_test = list()
    for i in range(35):
        data.set_experiment(i)
        x_train.append(data.get_channel(0))
    for i in range(35, 40):
        data.set_experiment(i)
        x_test.append(data.get_channel(0))

    x_train = np.array(x_train)
    x_test = np.array(x_test)

    # reshape signal into [samples, timesteps, features]
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    # data = DataProvider(file_number=1)
    # x_train = data.get_matrix(end=15, normalized=True)
    # x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    # x_test = data.get_matrix(start=16, normalized=True)
    # x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
    return x_train, x_test


# define signal sequence
signal_train, signal_test = get_dataset()
# signal_train = array([[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], [-.4, -.3, -.2, -.1, 0, .1, .2, .3, .4]])
# signal_train = array([[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]])
# signal_train = signal_train.reshape((len(signal_train), np.prod(signal_train.shape[1:])))

encoded_length = 8000
signal_length = len(signal_train[0])

input_signal = Input(shape=(signal_length, ))

# define encoder | representation
# model = Sequential()
# model.add(LSTM(
#     encoded_length,
#     activation='tanh',
#     activity_regularizer=regularizers.l1(10e-5),
#     input_shape=(signal_length, 1)))
#
# model.add(RepeatVector(signal_length))
# model.add(LSTM(signal_length, activation='tanh'))
# model.add(TimeDistributed(Dense(1)))

# define encoder | representation
encoded = Dense(
    encoded_length,
    activation='tanh',
    activity_regularizer=regularizers.l1(10e-10))(input_signal)
# define reconstruct decoder | reconstructions
decoded = Dense(signal_length, activation='tanh')(encoded)
model = Model(input_signal, decoded)
# this model maps an signal to its encoded representation
encoder = Model(input_signal, encoded)

encoded_input = Input(shape=(encoded_length,))
# retrieve the last layer of the autoencoder model
decoder_layer = model.layers[-1]
# create the decoder model
decoder = Model(encoded_input, decoder_layer(encoded_input))

model.compile(optimizer='rmsprop', loss='mse', metrics=['acc'])
model.fit(signal_train, signal_train, epochs=50, verbose=1)

# Original
# signal_test = array([
#     [.1, .2, .3, .4, .5, .6, .7, .8, .9],
#     [-.4, -.3, -.2, -.1, 0, .1, .2, .3, .4]])
# # signal_test = array([[.1, .2, .3, .4, .5, .6, .7, .8, .9]])
# signal_test = signal_test.reshape((len(signal_test), len(signal_test[0])))

# Reconstruction
# reconstructed = model.predict(signal_test)
# print(reconstructed)
#
# # encoding
# new_model = Model(inputs=model.inputs, outputs=model.layers[0].output)
# encoded = new_model.predict(signal_test)
# print(encoded)

# print(signal_test)
# encoded_vec = encoder.predict(signal_test)
# # print(encoded_vec)
# # reconstructed = decoder.predict(encoded_vec)
# # print(reconstructed)
# #
# # plot(signal_test, reconstructed, encoded_vec)
