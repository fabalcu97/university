from numpy import divide, array, linalg
from six.moves import cPickle as pickle

path = "/Users/fabalcu97/Downloads/DEAP/data_preprocessed_python/"


class DataProvider:
    def __init__(self, file_number, experiment=0):
        self.record = pickle.load(
            open(path + "s{n}.dat".format(n=file_number), 'rb'),
            encoding='latin1')
        self.labels = self.record['labels']
        self.data = self.record['data']
        self.experiment = experiment

    def set_experiment(self, experiment):
        """There are 40 experiments available. Each with 40 channels recorded."""
        self.experiment = experiment

    def get_matrix(self,start=0, end=31, normalized=False):
        """Get channels from 0 to end."""
        if end > 31:
            raise Exception(
                'DataProvider.get_matrix() => Maximum length is 31!!')
        if normalized:
            normalized_vec = list()
            for i in self.data[self.experiment][start:end]:
                normalized_vec.append(i / linalg.norm(i))
            return array(normalized_vec)
        return array(self.data[self.experiment][start:end])

    def get_channel(self, channel, normalized=True):
        """Get channel information, useful from 0 to 31. The difference, til 39, are different than eeg information."""
        data = self.data[self.experiment][channel]
        if normalized:
            # _min = min(data)
            # _max = max(data)
            # return [-1 + ((x - _min)*2)/(_max-_min) for x in data]
            return divide(data, max(data))
        return data

    def get_valence(self):
        """Valence experiment."""
        value = self.labels[self.experiment][0]
        if value <= 3.66:
            return -1
        elif 3.66 < value <= 6.33:
            return 0
        elif value > 6.33:
            return 1

    def get_arousal(self):
        """Arousal experiment."""
        value = self.labels[self.experiment][1]
        if value <= 3.66:
            return -1
        elif 3.66 < value <= 6.33:
            return 0
        elif value > 6.33:
            return 1
