from keras import regularizers
from keras.layers import Input, Dense
from keras.models import Model
import numpy as np
import matplotlib.pyplot as plt

from DataProvider import DataProvider


def plot(original, reconstructed, encoded):
    n = len(original)  # how many digits we will display
    rows = 3
    plt.figure(figsize=(50, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(rows, n, i + 1)
        plt.plot(x_test[i])
        plt.gray()
        # ax.set_ylim([-1, 1])

        # display reconstruction
        ax = plt.subplot(rows, n, i + 1 + n)
        plt.plot(decoded_imgs[i])
        plt.cool()
        # ax.set_ylim([-1, 1])

        ax = plt.subplot(rows, n, i + 1 + 2*n)
        plt.plot(encoded_imgs[i])
        plt.copper()
        # ax.set_ylim([-1, 1])
    plt.show()

def get_dataset():
    data = DataProvider(file_number=1)
    # For each experiment, extract one channel

    x_train = list()
    x_test = list()
    for i in range(35):
        data.set_experiment(i)
        x_train.append(data.get_channel(0))
    for i in range(35, 40):
        data.set_experiment(i)
        x_test.append(data.get_channel(0))

    x_train = np.array(x_train)
    x_test = np.array(x_test)
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    # data = DataProvider(file_number=1)
    # x_train = data.get_matrix(end=15, normalized=True)
    # x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    # x_test = data.get_matrix(start=16, normalized=True)
    # x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
    return x_train, x_test

# this is the size of our encoded representations
# encoding_dim = 1000  # 330 floats -> compression of factor 24.5, assuming the signal is 8064 floats
# signal_size = 8064  # Size of the signal

encoding_dim = 10
signal_size = 10

# this is our signal placeholder
input_img = Input(shape=(signal_size,))
# "encoded" is the encoded representation of the signal
encoded = Dense(encoding_dim, activation='sigmoid',
                activity_regularizer=regularizers.l1(10e-5))(input_img)
# "decoded" is the lossy reconstruction of the signal
decoded = Dense(signal_size, activation='sigmoid')(encoded)

# this model maps an signal to its reconstruction
autoencoder = Model(input_img, decoded)

# this model maps an signal to its encoded representation
encoder = Model(input_img, encoded)

encoded_input = Input(shape=(encoding_dim,))
# retrieve the last layer of the autoencoder model
decoder_layer = autoencoder.layers[-1]
# create the decoder model
decoder = Model(encoded_input, decoder_layer(encoded_input))

# autoencoder.compile(optimizer='adadelta', loss='mean_absolute_error')
autoencoder.compile(
    optimizer='adadelta',
    # optimizer='sgd',
    loss='binary_crossentropy',
    metrics=['accuracy'])
# autoencoder.compile(optimizer='adadelta', loss='mean_squared_logarithmic_error')

# x_train, x_test = get_dataset()
x_train = np.array([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]])
x_test = np.array([[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]])

x_train[0] = x_train[0] / np.linalg.norm(x_train[0])
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
x_test[0] = x_test[0] / np.linalg.norm(x_test[0])
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

autoencoder.fit(x_train, x_train,
                shuffle=True,
                epochs=50,
                validation_data=(x_test, x_test))
# encode and decode some digits
# note that we take them from the *test* set
encoded_imgs = encoder.predict(x_test)
print(encoded_imgs)
decoded_imgs = decoder.predict(encoded_imgs)
print(decoded_imgs)

plot(x_test, encoded_imgs, decoded_imgs)