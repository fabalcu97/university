from typing import List

from keras.layers import Input, Dense
from keras.models import Model
from pandas import DataFrame
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.model_selection import cross_val_score


from DataProvider import DataProvider


def plot(original, reconstructed, encoded):
    n = 1 or len(original)  # how many digits we will display
    rows = 2
    plt.figure(figsize=(10, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(rows, n, i + 1)
        plt.plot(original[i])
        plt.gray()
        # ax.set_ylim([-1, 1])

        # display reconstruction
        # ax = plt.subplot(rows, n, i + 1 + n)
        plt.plot(reconstructed[i])
        plt.cool()
        # ax.set_ylim([-1, 1])

        ax = plt.subplot(rows, n, i + 1 + n)
        plt.plot(encoded[i])
        plt.copper()
        # ax.set_ylim([-1, 1])
    plt.show()


def get_dataset(file: int = 1):
    data = DataProvider(file_number=file)
    x_train = list()
    valence_train = list()
    arousal_train = list()
    x_test = list()
    valence_test = list()
    arousal_test = list()
    for i in range(15):
        data.set_experiment(i)
        valence_train.append(data.get_valence())
        arousal_train.append(data.get_arousal())
        x_train.append(data.get_channel(0))
    for i in range(31):
        data.set_experiment(i)
        valence_test.append(data.get_valence())
        arousal_test.append(data.get_arousal())
        x_test.append(data.get_channel(0))

    x_train = np.array(x_train)
    x_test = np.array(x_test)
    return x_train, arousal_train, valence_train, x_test, arousal_test, valence_test


def get_duplicates_count(list_of_elements: List):
    """ Get frequency count of duplicate elements in the given list"""

    elements = dict()
    for elem in list_of_elements:
        elements[elem] = elements.get(elem, 0) + 1
    return elements


# def train_autoencoder(train_input, epochs=50, hidden_neurons=80, verbose=1):
#     shape_input = np.shape(train_input)
#     input_data_shape = Input(shape=(shape_input[1],))
#     output_layer_shape = shape_input[1]
#
#
#     # Encoder with {hidden_neurons} neurons
#     encoder = Dense(hidden_neurons, activation='tanh')(input_data_shape)
#
#     # Decoder from encoded signal size to real signal size
#     decoder = Dense(hidden_neurons, activation='tanh')(encoder)
#     decoder = Dense(output_layer_shape, activation='tanh')(decoder)
#     autoencoder = Model(input_data_shape, decoder)
#     # autoencoder.summary()
#     autoencoder.compile(optimizer='rmsprop', loss='mse', metrics=['acc'])
#
#     # Training
#     history = autoencoder.fit(
#         train_input, train_input, epochs=epochs, verbose=verbose)
#
#     # print(max(history.history.get('acc')))
#     # return history.history.get('acc')[-1]
#     return autoencoder

# signal_train, signal_test = get_dataset()
# input_data_shape = len(signal_test)
# this is the size of our encoded representations
# hidden_neurons = 500  # Encoded representation length
# accuracies = list()
# for hidden_neurons in range(100, 1000, 100):
#     for epochs in range(50, 300, 50):
#         print(f'Neurons: {hidden_neurons} - Epochs: {epochs}')
#         accuracies.append([hidden_neurons, epochs, train_autoencoder(
#             signal_train, epochs=epochs, hidden_neurons=hidden_neurons, verbose=0)])
# print(accuracies)


#  Tests

headers = ['user', 'AE acc', 'RBF.2_arousal', 'RBF.2_valence']
results = list()
# for test in range(1, 15):
for file in range(1, 2):
    print(f'============ {file} ============')
    signal_train, arousal_train, valence_train, signal_test, arousal_test, valence_test = get_dataset(file)
    # signal_train, _ = get_dataset(1)
    # _, signal_test = get_dataset(2)
    hidden_neurons = 200
    epochs = 200
    shape_input = np.shape(signal_train)
    input_data_shape = Input(shape=(shape_input[1],))
    output_layer_shape = shape_input[1]

    # Encoder with {hidden_neurons} neurons
    encoded = Dense(hidden_neurons, activation='tanh')(input_data_shape)
    # Decoder from encoded signal size to real signal size
    decoded = Dense(hidden_neurons, activation='tanh')(encoded)
    decoded = Dense(output_layer_shape, activation='tanh')(decoded)
    autoencoder = Model(input_data_shape, decoded)
    # autoencoder.summary()
    autoencoder.compile(optimizer='rmsprop', loss='mse', metrics=['acc'])
    # Training
    history = autoencoder.fit(
        signal_train, signal_train, epochs=epochs, verbose=0,
        batch_size=256, shuffle=True)
    accuracy = history.history.get('acc')[-1]

    # Separating encoder from decoder
    encoded_input = Input(shape=(hidden_neurons,))
    # retrieve the last layer of the autoencoder model
    decoder_layer = autoencoder.layers[-1]
    # create the decoder model
    test_decoder = Model(encoded_input, decoder_layer(encoded_input))

    # Testing
    encoder = Model(input_data_shape, encoded)
    train_encoded_vec = encoder.predict(signal_train)
    test_encoded_vec = encoder.predict(signal_test)
    reconstructed = test_decoder.predict(test_encoded_vec)
    plot(signal_test, reconstructed, test_encoded_vec)

    accuracies = dict()
    classifiers = {
        'RBF.2': SVC(kernel='rbf', gamma=.2, decision_function_shape='ovo'),
        # 'KNN': KNeighborsClassifier(n_neighbors=3),
        # 'RBF.05': SVC(kernel='rbf', gamma=.05, decision_function_shape='ovo'),
        # 'POLY': SVC(kernel='poly', gamma='auto', decision_function_shape='ovo',
        #             degree=5),
    }
    row = [file, accuracy]

    for classifier_key in classifiers.keys():
        # print('=====', classifier_key, '=====')
        min_arousal = min(get_duplicates_count(arousal_test).values())
        arousal_validation = cross_val_score(
            classifiers[classifier_key], test_encoded_vec, arousal_test,
            cv=2 if min_arousal < 2 else min_arousal)

        min_valence = min(get_duplicates_count(valence_test).values())
        valence_validation = cross_val_score(
            classifiers[classifier_key], test_encoded_vec, valence_test,
            cv=2 if min_valence < 2 else min_valence)
        # print(valence_validation)
        row.append(np.average(arousal_validation) * 100)
        row.append(np.average(valence_validation) * 100)
        # accuracies[f'{file}_arousal_{classifier_key}'] = np.average(arousal_validation) * 100
        # accuracies[f'{file}_valence_{classifier_key}'] = np.average(valence_validation) * 100
    print(row)
    results.append(row)
    print('============================')

print(results)
DataFrame(data=results).to_csv(f'./results.csv', header=headers, index=None)
