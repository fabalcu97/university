import numpy as np
from scipy import stats
from entropy import higuchi_fd as fractal_dimension

from bandPower import bandpower
from cConstants import FREQUENCIES, SAMPLING
from hjorthParams import hjorth


def get_feature_vector(data):
    """
    Get a list of dictionaries. Each one is a feature.

    [
        "median", "deviation", "kurtosis", "theta", "low_alpha", 
        "alpha",  "beta", "gamma", "activity", "mobility", "complexity", "fractal"
    ]
    [
        "v1", "v2", "v3",
        "v4", "v4", "v6",
        "v7", "v8", "v9",
        "v10", "v11", "v12"
    ]
    """

    fv = []
    median = np.median(data)
    fv.append(median)

    std_deviation = np.std(data)
    fv.append(std_deviation)

    kurtosis = stats.kurtosis(data)
    fv.append(kurtosis)

    for k, f_range in FREQUENCIES.items():
        fv.append(bandpower(data, SAMPLING, f_range[0], f_range[1]))

    activity, mobility, complexity = hjorth(data)
    fv.append(activity)
    fv.append(mobility)
    fv.append(complexity)

    f_dimension = fractal_dimension(data)
    fv.append(f_dimension)

    return fv


def discretize_features(features):
    for i in range(1, features.shape[1]):
        feature_vec = np.array(features[:, i:i+1][1:], dtype=float)
        feature_vec = np.ravel(feature_vec)
        m = np.median(feature_vec)
        d = np.std(feature_vec)
        for idx, feature in enumerate(feature_vec):
            if feature >= m + (d/2):
                features[idx+1, i] = 1
            elif m - (d/2) <= feature or feature < m + (d/2):
                features[idx+1, i] = 0
            elif feature < m - (d/2):
                features[idx+1, i] = -1
