from subprocess import run, PIPE

executable = '/Users/fabalcu97/Repositories/University-env/university/2019-II/Thesis/mRMR/fast-mRMR/utils/data-reader/mrmr-reader'
files = '/Users/fabalcu97/Repositories/University-env/university/2019-II/Thesis/mRMR/output/'


def csvToBinaryMrmr(fileName: str):
    res = run(
        [executable, files + fileName + '.csv'],
        stdout=PIPE)
    if res.returncode != 0:
        print("ERROR =>", res.stdout)
