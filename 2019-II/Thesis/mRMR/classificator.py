import numpy as np
from pandas import read_csv, DataFrame
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score

from feature_selection import fast_feature_selection, old_feature_selection
from utils import get_columns, get_duplicates_count
from cConstants import (
    EXPERIMENTS,
    CANDIDATES,
)

dimensions = ['arousal', 'valence']
classifiers = {
    'RBF.2': SVC(kernel='rbf', gamma=.2, decision_function_shape='ovo'),
    'RBF.05': SVC(kernel='rbf', gamma=.05, decision_function_shape='ovo'),
    'POLY': SVC(kernel='poly', gamma='auto', decision_function_shape='ovo', degree=5),
}

for dimension in dimensions:
    print(f'======== Dimension {dimension} ========')
    for classifier_key in classifiers.keys():
        print(f'======== Classifier {classifier_key} ========')
        headers = ['user', 10, 20, 30, 40, 50, 60, 70, 80, 90]
        tmp_results = []
        for user in range(CANDIDATES):
            print(f'======== User {user} ========')
            accuracies = [user]
            for i in range(10, 100, 10):
                print(f'======== {i} Features ========')
                filename = f'{dimension}_{user}'
                filepath = f'./output/{filename}.csv'
                main_dataset = read_csv(filepath)
                features = old_feature_selection(main_dataset, features=i)
                # print(features)
                # features = fast_feature_selection(
                #     filename, features=i, headers=list(main_dataset.columns))
                # print(features)
                # exit(0)
                dataset = get_columns(main_dataset, features)
                x = dataset.iloc[:, 1:].values
                y = np.ravel(dataset.iloc[:, :1].values)
                classes_reps = list(get_duplicates_count(y).values())
                classes_reps.sort()
                cv_min = classes_reps[0]
                if classes_reps[0] > 10:
                    cv_min = 10
                if classes_reps[0] < 2:
                    cv_min = 2
                cf_validation = cross_val_score(
                    classifiers[classifier_key], x, y, cv=cv_min)
                accuracies.append(np.average(cf_validation) * 100)
                # print('==========')
                # print(cv_min, cf_validation, np.average(cf_validation) * 100)
                # print(accuracies)
                # print('==========')
            tmp_results.append(accuracies)
        DataFrame(data=tmp_results).to_csv(
            f'./accuracy/{dimension}_{classifier_key}.csv', header=headers, index=None)