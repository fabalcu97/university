import re
from typing import List


def get_header(channel):
    return [f"{channel}_median", f"{channel}_deviation", f"{channel}_kurtosis", f"{channel}_theta",
            f"{channel}_low_alpha", f"{channel}_alpha", f"{channel}_beta", f"{channel}_gamma",
            f"{channel}_activity", f"{channel}_mobility", f"{channel}_complexity", f"{channel}_fractal"]


def get_columns(pd_dataset, columns=[]):
    return pd_dataset[['class'] + columns]


def get_duplicates_count(list_of_elements: List):
    """ Get frequency count of duplicate elements in the given list"""

    elements = dict()
    for elem in list_of_elements:
        elements[elem] = elements.get(elem, 0) + 1
    return elements
