import pymrmr
from subprocess import run, PIPE
from typing import List

executable = '/Users/fabalcu97/Repositories/University-env/university/2019-II/Thesis/mRMR/fast-mRMR/cpu/src/fast-mrmr'
files = '/Users/fabalcu97/Repositories/University-env/university/2019-II/Thesis/mRMR/fast-mRMR/utils/data-reader/out/'


def fast_feature_selection(file_name: str, features: int = 10, headers: List = []):
    res = run(
        [executable, '-f', files + file_name + '.mrmr', '-a', str(features)],
        stdout=PIPE)
    stdout = res.stdout.decode('utf-8').replace('\n', '')
    features = [int(f) for f in stdout.split(',')]

    if headers:
        return [headers[i] for i in features]
    return features


def old_feature_selection(data, features=10):
    return pymrmr.mRMR(data, "MID", features)
