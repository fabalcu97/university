FREQUENCIES = {
    'theta': (4, 8, ),
    'low_alpha': (8, 10, ),
    'alpha': (8, 12, ),
    'beta': (12, 30, ),
    'gamma': (30, 45, ),
}

CHANNELS_INDEX = {
    'f3': 2,
    'f4': 19,
    'f7': 3,
    'f8': 20,
    'fc1': 5,
    'fc2': 22,
    'fc5': 4,
    'fc6': 21,
    'fp1': 0,
    'fp2': 16,
    'af3': 1,
    'af4': 17,
    'c3': 6,
    'c4': 24,
}

SAMPLING = 128
CHANNELS = 32
CANDIDATES = 32
EXPERIMENTS = 40

TOTAL_FRAMES = 60
WINDOW_TIME_2 = 2
WINDOW_TIME_4 = 4

COLUMNS = ["class", "v1", "v2", "v3", "v4", "v4",
           "v6", "v7", "v8", "v9", "v10", "v11", "v12"]
