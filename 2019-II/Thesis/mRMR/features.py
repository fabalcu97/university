import numpy as np
from pandas import DataFrame

from DataProvider import DataProvider
from csvToMrmr import csvToBinaryMrmr
from feature_extraction import get_feature_vector, discretize_features
from cConstants import CANDIDATES, CHANNELS_INDEX, EXPERIMENTS
from utils import get_header

dimensions = ['arousal', 'valence']
headers = []
for key, value in CHANNELS_INDEX.items():
    headers.extend(get_header(key))

for user in range(CANDIDATES):
    print(f"========= User {user} =========")
    data_provider = DataProvider(user + 1)
    for dimension in dimensions:
        print(f"========= Dimension {dimension} =========")
        features = np.array([['class'] + headers])
        for experiment in range(EXPERIMENTS):  # videos
            # print(f"========= Experiment {experiment} =========")
            data_provider.set_experiment(experiment)
            _class = getattr(data_provider, f'get_{dimension}')()
            tmp = list()
            for key, value in CHANNELS_INDEX.items():
                # print(f"========= Channel {key} =========")
                channel = data_provider.get_channel(value)
                tmp.extend(get_feature_vector(channel))
            features = np.append(features, [[_class] + tmp], axis=0)
            del tmp
        discretize_features(features)
        df = DataFrame(data=features)
        file_name = f'{dimension}_{user}'
        df.to_csv(f'./output/{file_name}.csv', header=None, index=None)
        csvToBinaryMrmr(file_name)
        del features
    del data_provider
