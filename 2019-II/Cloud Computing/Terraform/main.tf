provider "aws" {
  profile = "default"
  region  = "us-east-1"
  version = "~> 2.38"
}

resource "aws_placement_group" "test" {
  name     = "test"
  strategy = "partition"
}

resource "aws_launch_configuration" "test" {
  image_id = "ami-b374d5a5"
  instance_type = "t2.micro"
  name = "test-launch-config"
}

resource "aws_subnet" "test" {
  vpc_id = "vpc-8f287ee9"
  cidr_block = "172.31.80.0/20"
}

resource "aws_autoscaling_group" "test" {
  name                      = "cloud-terraform-test"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 60
  health_check_type         = "ELB"
  desired_capacity          = 3
  force_delete              = true
  placement_group           = "${aws_placement_group.test.id}"
  launch_configuration      = "${aws_launch_configuration.test.name}"
  vpc_zone_identifier       = ["${aws_subnet.test.id}"]
}