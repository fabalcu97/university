#!/usr/bin/env python3
import sys

if __name__ == "__main__":
    index = dict()

    for line in sys.stdin:
        word, document = line.split(',')
        index[word] = index[word].append(document) if index[word] else list()

    for word, documents in index.items():
        print(f'{word},{documents}')
