#!/urs/bin/python3

import fileinput


if __name__ == "__main__":
    for line in fileinput.input():
        for words in line.split():
            for word in words:
                if isinstance(word, str):
                    print(f'{word},{fileinput.filename()}')
