const express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var request = require('request-promise-native');

const app = express();
const port = 3000;

let vmLoadTable = require('./VMLoadTable');
let vmResourcesTable = require('./VMResources');

app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let loadTable = new vmLoadTable();
let resourcesTable = new vmResourcesTable();

app.post('/new-resource', (req, res, next) => {
  let resInfo = req.body.information;
  resourcesTable.insertResource(resInfo).then(v => {
    let msg = `The resource was ${v ? '' : 'not'}added`;
    res.send(msg);
  });
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

function getStatus() {
  // TODO: Iterate over all the instances registered in the resources table.
  request.get('http://localhost:3001/get-status').then(res => {
    console.log(res);
  }).catch(err => {
    console.error(err.body);
  })
}

// interval = setInterval(getStatus, 10000);

// clearInterval(interval);

/**
 * Retrieve VM status every 30 seconds.
 * Register all the VM information related.
 * Calculate fitness value to determine wether migrate or not.
 *
 */