
class VMLoadTable {

  constructor() {
    if (this) {
      return this;
    }
  }

  vmLoadTable = [];

  isEmpty = () => {
    return Boolean(this.vmLoadTable.length);
  }

};

module.exports = VMLoadTable;