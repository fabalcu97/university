
class VMResourcesTable {

  constructor() {
    if (this) {
      return this;
    }
  }

  /**
   * This contains the Id, Memory, CPULoad, etc.
   */
  vmResourcesTable = [];

  isEmpty = () => {
    return Boolean(this.vmResourcesTable.length);
  };

  insertResource = (resourceInformation) => {
    return new Promise((resolve, reject) => {
      this.vmResourcesTable.push(resourceInformation);
      resolve(true);
    });
  };

};

module.exports = VMResourcesTable;