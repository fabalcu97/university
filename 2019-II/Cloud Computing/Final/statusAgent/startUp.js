var request = require('request-promise-native');

const loadAgent = 'http://localhost:3000';

function startUp() {
  // Notify existence
  request.post(`${loadAgent}/new-resource`).then(response => {
    console.log(response);
  }).catch(err => {
    console.error(err);
  });

}

module.exports = startUp;