const express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var startUpMethods = require('./startUp');

const app = express();
const port = 3001;

app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/get-status',  (req, res, next) => {
  let msg = `The resource sent its system information`;
  res.send(msg);
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

startUpMethods();