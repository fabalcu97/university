#include <iostream>

using namespace std;

class Polygon
{
private:
    int width, height;

public:
    Polygon(int a, int b) : width(a), height(b) {}
    int area(void){return 0};
    void printarea()
    {
        cout << this->area() << '\n';
    }
}

class ArrayPolygon
{
private:
    Polygon *arr;
    int size;

    // Default
    ArrayPolygon(){};

    // Overloaded
    ArrayPolygon(Polygon *array, int size)
    {
        this.size = size;
        this.arr = array;
    };

    // Copy
    ArrayPolygon(const ArrayPolygon &copyAP)
    {
        this.size = copyAP.size;
        this.arr = copyAP.arr;
    }

    // Destructor
    ~ArrayPolygon()
    {
        delete this.size;
        delete this.arr;
        cout << "Destroyed!" << endl;
    }

    void resize(int newSize)
    {
        if (newSize < 0)
        {
            cout << "New Size is invalid!" << endl;
            return;
        }
        // Reduction
        if (newSize < this.size)
        {
            for (int i = newSize; i < this.size; ++i)
            {
                delete this.arr[i];
            }
            // for (Polygon *i = this.arr[newSize]; i != nullptr; ++i)
            // {
            //     delete i;
            // }
        }
        // Increase
        else if (newSize > this.size)
        {
            Polygon *newPtr[newSize];
            Pointer *oldArr = this.arr;
            for (int i = 0; i < this.size; ++i)
            {
                newPtr[i] = *oldArr[i];
                delete oldArr[i];
            }
            this.arr = newPtr;
        }
        this.size = newSize;
    }

    void append(Polygon *newPolygon)
    {
        this.resize(this.size + 1);
        this.arr[this.size - 1] = newPolygon;
    }

    void insert(int position, Polygon *newPolygon)
    {
        this.resize(this.size + 1);
        for (int i = this.size - 1; i > position + 1; --i)
        {
            this.arr[i] = this.arr[i - 1];
        }
        this.arr[postion] = newPolygon;
    }

    void delete (int position)
    {
        this.size -= 1;
        Polygon *newArr[this.size];
        delete this.arr[position];
        for (int i, j = 0; i < this.size; ++i, ++j)
        {
            if (this.arr[i] == nullptr)
            {
                j += 1;
            }
            newArr[i] = this.arr[j]
        }
    }

public:
    int getSize()
    {
        return this.size;
    }
}