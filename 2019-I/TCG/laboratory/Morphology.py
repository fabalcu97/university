from numpy import zeros, array_equal, divide, logical_or, multiply, array
from cv2 import imwrite
from time import sleep


class Morphology:
    """Morphological operations on an image."""

    def __init__(self, image, structure, origin=None):
        self.image = image
        self.rows = len(self.image)
        self.columns = len(self.image[0])
        self.structure = structure
        self.origin = origin
        if not origin:
            self.origin = (int(len(self.structure)/2),
                           int(len(self.structure[0])/2))
        self.new_image = zeros((self.rows, self.columns))

    def __process_image(self, operation):
        image = self.image
        up_bottom = int((len(self.structure) - 1) / 2)
        left_right = int((len(self.structure[0]) - 1) / 2)
        for i in range(up_bottom, self.rows - up_bottom):
            for j in range(left_right, self.columns - left_right):
                # print("==============================")
                # print(i, j)
                window = image[i - up_bottom:i +
                               up_bottom + 1, j - left_right:j + left_right + 1]
                # print("IW =>", image[i - up_bottom:i +
                #                      up_bottom + 1, j - left_right:j + left_right + 1])
                operation(window, i, j)

    def __erode(self, window, i, j):
        window = divide(window, 255)
        if self.or_op(window):
            # print("W =>", window)
            x = self.origin[0] - int(len(self.structure)/2)
            y = self.origin[1] - int(len(self.structure[0])/2)
            self.new_image[i + x][j + y] = 255
            # print("======= Write  =======")
            # imwrite("./output/img.png", self.new_image)
            # sleep(1)

    def or_op(self, window):
        for i in range(len(self.structure)):
            for j in range(len(self.structure[0])):
                if self.structure[i][j] == 1 and window[i][j] == 0:
                    return False
        return True

    def erode(self):
        self.__process_image(self.__erode)

    def __dilate(self, window, i, j):
        window = divide(window, 255)

        # print("W =>", window)
        # print("S =>", self.structure)
        # if i == 70:
        #     exit(0)
        if window[self.origin[0]][self.origin[1]] != 0:
            up_bottom = int((len(self.structure) - 1) / 2)
            left_right = int((len(self.structure[0]) - 1) / 2)
            replacement = multiply(logical_or(window, self.structure), 255)
            # print("R =>", replacement)
            self.new_image[i - up_bottom:i +
                           up_bottom + 1, j - left_right:j + left_right + 1] = replacement
            # print("======= Write  =======")
            # imwrite("./output/img.png", self.new_image)
            # sleep(1)

    def dilate(self):
        self.__process_image(self.__dilate)

    def opening(self):
        self.erode()
        self.image = array(self.new_image)
        self.dilate()

    def closing(self):
        self.dilate()
        self.image = array(self.new_image)
        self.erode()