from cv2 import imread, imwrite, waitKey, destroyAllWindows
from multiprocessing import Process
from numpy import zeros, cos, sin, array, ones

from time import time

filename = './input/lena.jpg'

image = imread(filename)
rows, columns, channels = image.shape
new_image = zeros((rows, columns, 3), dtype=int)

translate_pixels = 50

for i in range(rows):
    for j in range(columns):
        new_i = min(i + translate_pixels, rows - 1)
        new_j = min(j + translate_pixels, columns - 1)
        new_image[new_i, new_j] = image[i, j]

imwrite(__file__.split('.')[0] + '.jpg', new_image)