#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from numpy import ones, dot, zeros, array

from image_reader import ImageReader, COLOR_AMOUNT
from plot import save_histogram_plot

filename = "coins.png"

image = ImageReader(filename, _filter=[], mask_dimensions=(2, 2))

histogram = image.get_histogram()

new_colors = [[] for i in range(COLOR_AMOUNT)]
color_amount = len(histogram)
prop_sum = 0.0
for color in histogram:
    color_prop = len(color) / float(image.total_pixel)
    prop_sum += color_prop
    new_color = int(round((COLOR_AMOUNT - 1) * prop_sum))
    new_colors[new_color] += color

for color, pixel_array in enumerate(new_colors, 0):
    for pixel in pixel_array:
        image[pixel] = color

image.write()

save_histogram_plot(histogram, name='original')
save_histogram_plot(new_colors, name='new')



# %load_ext autoreload
# %autoreload 2
# from image_reader import ImageReader