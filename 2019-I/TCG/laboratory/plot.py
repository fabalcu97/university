import matplotlib.pyplot as plt

def save_histogram_plot(colors=[], name='hg'):

    n, bins, patches = plt.hist(colors, facecolor='g')

    plt.xlabel('Colors')
    plt.ylabel('Reps')
    plt.title('Histogram of image')
    plt.axis([0, 256, 0, 300])
    plt.grid(True)
    plt.savefig('./output/hg_' + name + '.png')

