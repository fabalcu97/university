from cv2 import imread, imshow, waitKey, destroyAllWindows, imwrite
from multiprocessing import Process
from numpy import zeros, ceil, ones

from time import time

filename = 'fabri.jpg'

image = imread('./signal/' + filename)
rows, columns, channels = image.shape

factor = 5
new_rows = int(rows * factor)
new_columns = int(columns * factor)

new_image = zeros((new_rows, new_columns, 3), dtype=int)


for i in range(new_columns):
    for j in range(new_rows):
        src_i = min(int(i / factor), columns - 1)
        src_j = min(int(j / factor), rows - 1)
        new_image[i, j] = image[src_i, src_j]


imwrite('./output/' + filename.split('.')[0] + '.jpg', new_image)
