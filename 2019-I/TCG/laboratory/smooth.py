from numpy import array, dot, add

from image_reader import ImageReader

filename = 'circuit.jpg'
# filename = 'fabri.jpg'
# filename = 'fabri_gs_eq.jpg'
# filename = 'church1_bn.jpg'

IDENTITY = array([[0, 0, 0], [0, 1, 0], [0, 0, 0]])
PONDERATE = array([[0, 1, 0], [1, 2, 1], [0, 1, 0]])
LAPLACE = array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])
ROBERTS = array([[-1, -1], [1, 1]])
SOBEL = array([[2, 2, 0], [2, 0, -2], [0, -2, -2]])
PREWITT = array([[-2, -1, 0], [-1, 0, 1], [0, 1, 2]])
HORIZONTAL = array([[-1, -1, -1], [2, 2, 2], [-1, -1, -1]])
VERTICAL = array([[-1, 2, -1], [-1, 2, -1], [-1, 2, -1]])
POSITIVE_45 = array([[-1, -1, 2], [-1, 2, -1], [2, -1, -1]])
NEGATIVE_45 = array([[2, -1, -1], [-1, 2, -1], [-1, -1, 2]])
SUM_FILTER = VERTICAL + HORIZONTAL + POSITIVE_45 + NEGATIVE_45
POINT = array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
# FILTER = PREWITT
# FILTER = SOBEL
# FILTER = LAPLACE
# FILTER = ROBERTS
# FILTER = PONDERATE
FILTER = NEGATIVE_45
# FILTER = HORIZONTAL
# FILTER = VERTICAL
# FILTER = POSITIVE_45
# FILTER = SUM_FILTER

mask_dimensions = (3, 3)
# mask_dimensions = (2, 2)

image = ImageReader(filename, FILTER, mask_dimensions)
left_right = image.left_right
up_bottom = image.up_bottom
# for i in range(up_bottom, image.rows - up_bottom):
#     for j in range(left_right, image.columns - left_right):
#         window = image.image[i - up_bottom:i + up_bottom + 1, j - left_right:j + left_right + 1]
#         # image[i, j] = image.ponderated_prom(window)
#         # image[i, j] = image.median_value(window)
#         # image[i, j] = image.max_value(window)
#         # image[i, j] = image.min_value(window)
#         image[i, j] = image.gaussian(window)

# for i in range(1, image.rows - 1):
#     for j in range(1, image.columns - 1):
#         window = image.image[i - 1:i + 1, j - 1:j + 1]
#         sum = dot(FILTER[0], window[0])
#         sum += dot(FILTER[1], window[1])
#         image[i, j] = sum if sum > 0 else 0


for i in range(up_bottom, image.rows - up_bottom):
    for j in range(left_right, image.columns - left_right):
        window = image.image[i - up_bottom:i + up_bottom + 1, j - left_right:j + left_right + 1]
        image[i, j] = image.border(window)

image.write()


# %load_ext autoreload
# %autoreload 2