from numpy import empty, divide, arccos, sqrt, square, matmul, multiply, deg2rad, rad2deg
from image_reader import ImageReader, IMREAD_COLOR

filename = 'lena.jpg'

image = ImageReader(filename, _filter=[],
                    mask_dimensions=(2, 2), mode=IMREAD_COLOR)


def to_cmy(R, G, B):
    # return [ R, G, B]
    return [1 - R, 1 - G, 1 - B]


def to_hsv(R, G, B):
    M = max(R, G, B)
    m = min(R, G, B)
    v = M
    s = ((M - m) / M) if M != 0 else 0
    c = 60 / (M - m)
    h = 0
    if M == R:
        h = ((G - B) * c)
    elif M == G:
        h = ((B - R) * c) + 120
    elif M == B:
        h = ((R - G) * c) + 240
    if h < 0:
        h += 360
    return [h / 360, s, v]


def to_hsi(R, G, B):
    theta = rad2deg(
        arccos(
            (
                (
                    (R - G) + (R - B)
                ) / (
                    2 * sqrt(
                        square(R - G) + ((R - B) * (G - B))
                    )
                )
            )
        )
    )
    h = theta
    if B > G:
        h = 360 - theta
    s = 1 - ((3/(R + G + B)) * min(R, G, B))
    i = (1 / 3) * (R + G + B)

    return [h / 360, s, i]


def to_yuv(R, G, B):
    default = [
        [.299, .587, .144],
        [-.147, -.289, .436],
        [.615, -.515, -0.100],
    ]
    return matmul(default, [R, G, B])


def to_yiq(R, G, B):
    default = [
        [.299, .587, .144],
        [.596, -.275, -.321],
        [.212, -.523, .311],
    ]
    return matmul(default, [R, G, B])


KR = .299
KG = .587
KB = .114


def to_ycbcr(R, G, B):
    y = (KR * R) + (KG * G) + (KB * B)
    pb = (1/2) * ((B - y) / (1 - KB))
    pr = (1/2) * ((R - y) / (1 - KR))
    return [y, pb, pr]


def new_color_model(matrix, color_op):
    for i, row in enumerate(matrix, 0):
        for j, cell in enumerate(row, 0):
            # hsv = color_op(*divide(cell, 255))
            # print(hsv)
            # hsv[0] = (hsv[0] * 255) / 360
            # hsv[1] *= 255
            # hsv[2] *= 255
            # image[i, j] = list(hsv)
            image[i, j] = list(multiply(color_op(*divide(cell, 255)), 255))


# new_color_model(image.image, to_cmy)
# new_color_model(image.image, to_hsv)
# new_color_model(image.image, to_hsi)
new_color_model(image.image, to_yuv)
# new_color_model(image.image, to_yiq)
# new_color_model(image.image, to_ycbcr)

# image.write()
image.write_channels()
