from cv2 import imread, imwrite, waitKey, destroyAllWindows
from multiprocessing import Process
from numpy import zeros, cos, sin, array, radians

from time import time

filename = 'fabri.jpg'

image = imread('./signal/' + filename)
rows, columns, channels = image.shape
new_image = zeros((rows, columns, 3), dtype=int)

x0 = rows / 2
y0 = columns / 2

def rotate(angle, x, y):
#     newX = int(abs((x * cos(angle)) - (y * sin(angle))))
#     newY = int(abs((x * sin(angle)) + (y * cos(angle))))
    newX = int(abs(((x - x0) * cos(angle)) + ((y - y0) * sin(angle) + x0)))
    newY = int(abs((-(x - x0) * sin(angle)) + ((y - y0) * cos(angle) + y0)))
#     print("X=>", (x * cos(angle)) - (y * sin(angle)))
#     print("Y=>", (x * sin(angle)) + (y * cos(angle)))
    return (newX, newY,)



start = time()
angle = radians(-90)

for i in range(rows):
    for j in range(columns):
        x, y = rotate(angle, i, j)
        if x >= rows or y >= columns:
                continue
        new_image[x, y] = image[i, j]

print(time() - start)

imwrite('./output/' + filename.split('.')[0] + '.jpg', new_image)