from numpy import vectorize, exp, e
from math import log
from image_reader import ImageReader

filename = 'fabri.jpg'

image = ImageReader(filename, _filter=[], mask_dimensions=(2, 2))

def inverse(pixel):
    return  255 - pixel


def exponential(pixel_value, exp_val=2):
    return (float(pixel_value / 255) ** float(exp_val)) * 255

def logarithm(pixel_value, base=2):
    return log(1 + (pixel_value / 255), base) * 255

for i in range(image.rows):
    for j in range(image.columns):
        # image[i, j] = exponential(image[i, j], 10)
        # image[i, j] = logarithm(image[i, j], e)
        image[i, j] = inverse(image[i, j])


image.write()




# %load_ext autoreload
# %autoreload 2
# from image_reader import ImageReader