from numpy import array
from time import sleep

from image_reader import ImageReader
from Morphology import Morphology
from structures import SQUARE_3, SQUARE_7, LINE, CROSS, DIAMOND, SQUARE_5

# from numpy import zeros
# sq_img = zeros((500, 500))
# for i in range(100, 400):
#     for j in range(100, 400):
#         sq_img[i][j] = 255

# filename = 'closing.png'
# filename = 'opening.png'
# filename = 'j.png'
# treshold = 100
filename = 'coins.png'
treshold = 100
# filename = 'circuit.jpg'
# treshold = 60

image = ImageReader(filename, [], (3, 3))
image.set_binary(treshold=treshold)
image.new_image = image.binary_image
image.write()
# sleep(1)

morp = Morphology(image.binary_image, SQUARE_5)

# morp.erode()
# morp.dilate()
# morp.opening()
morp.closing()
image.new_image = morp.new_image

image.write()
