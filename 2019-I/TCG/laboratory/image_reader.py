from cv2 import (
    imread,
    imdecode,
    imencode,
    imwrite,
    threshold,
    THRESH_BINARY,
    THRESH_OTSU,
    IMREAD_GRAYSCALE,
    IMREAD_UNCHANGED,
    IMREAD_COLOR,
)
from numpy import ones, dot, zeros, array, median, pi, e, exp, empty

COLOR_AMOUNT = 256


class ImageNotFound(Exception):
    pass


class ImageReader():

    image_path = "./input/"

    def __init__(self, image_name, _filter, mask_dimensions, sigma=0.84089642,
                 mode=IMREAD_GRAYSCALE, *args, **kwagrs):
        exception_str = "Image '{name}' not found".format(
            name=self.image_path + image_name)

        self.name = image_name.split('.')[0]
        self.extension = image_name.split('.')[1]
        self.image = imread(self.image_path + image_name, mode)
        self.filter = _filter
        self.mask_dimensions = mask_dimensions
        self.left_right = int((self.mask_dimensions[1] - 1) / 2)
        self.up_bottom = int((self.mask_dimensions[0] - 1) / 2)
        self.gaussian_filter = self.get_gaussian_mask(sigma=0.84089642)

        if self.image is None:
            raise ImageNotFound(exception_str)

        self.rows = self.image.shape[0]
        self.columns = self.image.shape[1]
        self.channels = self.image.shape[2] if len(
            self.image.shape) == 3 else 1
        self.total_pixel = self.rows * self.columns

        self.new_image = zeros((self.rows, self.columns, self.channels))

    def __getitem__(self, key):
        return self.image[key]

    def __setitem__(self, key, value):
        self.new_image[key] = value

    def __str__(self):
        return str(self.image)

    def set_binary(self, treshold=127):
        (thresh, self.binary_image) = threshold(self.image, treshold, 255, THRESH_BINARY)

    def write(self):
        for i in range(self.channels):
            imwrite("./output/{name}_{c}.{ext}".format(name=self.name,
                                                       ext=self.extension, c=i), self.new_image)

    def channel_matrix(self, channel):
        temporal_matrix = zeros((self.rows, self.columns, 3))
        for i in range(self.rows):
            for j in range(self.columns):
                temporal_matrix[i, j, channel] = self.new_image[i, j][channel]
        return temporal_matrix

    def write_channels(self):
        for c in range(self.channels):
            temporal_matrix = self.channel_matrix(c)
            imwrite("./output/{name}_{ch}.{ext}".format(name=self.name,
                                                        ext=self.extension, ch=c), temporal_matrix)
        imwrite("./output/{name}.{ext}".format(name=self.name,
                                                        ext=self.extension), self.new_image)

    def get_histogram(self):
        if hasattr(self, "histogram") and self.histogram:
            return self.historgram
        self.histogram = [[] for i in range(COLOR_AMOUNT)]
        for i in range(self.rows):
            for j in range(self.columns):
                self.histogram[self.image[i, j]].append((i, j))
        return self.histogram

    def ponderated_prom(self, window):
        filter_sum = self.filter.sum() if self.filter.sum() != 0 else 1
        sum = 0
        for i in range(self.mask_dimensions[0]):
            sum += dot(self.filter[i], window[i])
        return sum / filter_sum

    def gaussian(self, window):
        filter_sum = self.gaussian_filter.sum() if self.gaussian_filter .sum() != 0 else 1
        sum = 0
        for i in range(self.mask_dimensions[0]):
            sum += dot(self.gaussian_filter[i], window[i])
        return sum / filter_sum

    def border(self, window):
        sum = 0
        for i in range(self.mask_dimensions[0]):
            sum += dot(self.filter[i], window[i])
        return 0 if sum < 0 else sum

    def median_value(self, window):
        l = window.reshape(len(window)*len(window[0]))
        l.sort()
        return int(median(l))

    def max_value(self, window):
        return window.max()

    def min_value(self, window):
        return window.min()

    def get_gaussian_mask(self, sigma):
        base = (1/(float(2*pi*(sigma**2))))
        mask = zeros(self.mask_dimensions)
        for i in range(-self.up_bottom, self.up_bottom + 1):
            for j in range(-self.left_right, self.left_right + 1):
                mask[i + self.up_bottom, j + self.left_right] = base * \
                    exp(-((i**2 + j**2)/(2*(sigma**2))))
        return mask
