from numpy import array

SOBEL = array([
    [5, 5, 0],
    [5, 0, -5],
    [0, -5, -5]
])
COLOR_AMOUNT = 256
BORDER_TRESH = 250