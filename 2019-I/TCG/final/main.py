import sys

import numpy as np
from cv2 import subtract
from scipy.ndimage.morphology import (
    grey_closing,
    grey_opening,
)

from borders import  get_border_width, get_borders_average
from image_reader import ImageReader
from binarization import binarize_image

filename = f'{sys.argv[1]}.bmp'
image = ImageReader(filename)
original_image = image.image
new_image = image.new_image

# Get average stroke width from borders
stk_avg = get_borders_average(original_image)

# The structure with an average size of the 3 highest strokes is used to separate background from foreground
structure_size = 2 * stk_avg + 1
structure = np.ones((structure_size, structure_size))
background_image = grey_closing(original_image, structure=structure)
background_image = grey_opening(background_image, structure=structure)

# Removing background
new_image = np.array(np.subtract(
    255, subtract(background_image, original_image)))
image.new_image = new_image
image.write()

# Binarization by Quad-Tree and KNN
binarize_image(new_image, image)


image.new_image = new_image
image.write()
