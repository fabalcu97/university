import numpy as np

from cv2 import subtract, Sobel, CV_64F
from image_reader import ImageReader
from constants import BORDER_TRESH


def get_border_width(row, width_list) -> dict:
    for idx in range(1, len(row) - 1):
        pixel = row[idx]
        counter = 0
        if pixel >= BORDER_TRESH and row[idx - 1] < BORDER_TRESH:
            while row[idx + 1] > BORDER_TRESH:
                counter += 1
                idx += 1
                pixel = row[idx]
        width_list[counter] = 1 + width_list.get(counter, 0)
    return width_list

def get_borders_average(image):
    border_image = Sobel(image, CV_64F, 1, 1, ksize=5)
    widths = dict()  # {'width': 'frecuencies'}
    for row in border_image:
        get_border_width(row, widths)
    widths.pop(0)
    maxs = sorted(widths.items(), key=lambda x: (x[1],x[0]), reverse=True)[:3]
    avg = list()
    for m in maxs:
        avg.append(m[0])
    stk_avg = int(np.average(avg))
    return 8 if stk_avg < 6  else stk_avg


# %load_ext autoreload
# %autoreload 2
