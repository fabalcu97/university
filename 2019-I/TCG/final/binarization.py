import numpy as np
from sklearn.cluster import KMeans


alpha = 1.3  # [1.3, 1.5]
beta = 8  # [3, 5]


def process_block(block):
    mx = np.max(block)
    mn = np.min(block)
    rng = mx - mn
    avg = np.mean(block)
    std = np.std(block)

    if (avg >= (mn + rng / alpha)) and (std <= beta):
        cluster_block(block, condition=False)
    else:
        cluster_block(block, condition=True)


def cluster_block(block, condition=True):
    print(condition)
    if not condition:
        block[:, :] = 255
        return
    cluster = KMeans(n_clusters=2, init=np.array([
        [0, 0, 100],
        [255, 255, 0]
    ]))
    characteristics = list()
    indexes = list()
    for i in range(1, len(block) - 1):
        for j in range(1, len(block[0]) - 1):
            window = block[i - 1:i + 2, j - 1:j + 2]
            neighbors = [window[0, 0], window[0, 1], window[0, 2], window[1, 0],
                         window[1, 2], window[2, 0], window[2, 1], window[2, 2]]
            c = [
                window[1, 1],
                np.mean(neighbors),
                np.std(neighbors)
            ]
            characteristics.append(c)
            indexes.append((i, j))
    predictions = cluster.fit_predict(characteristics)
    for idx, c in enumerate(predictions):
        x, y = indexes[idx]
        block[x, y] = c * 255


def binarize_image(image, img_obj):
    divs = 4
    height = len(image)
    width = len(image[0])
    h_len = int(np.floor(height / divs))
    w_len = int(np.floor(width / divs))
    blocks = list()
    for i in range(divs):
        i_w = i * w_len
        f_w = (i + 1) * w_len
        for j in range(divs):
            i_h = j * h_len
            f_h = (j + 1) * h_len
            # blocks.append(image[i_h:f_h, i_w:f_w])
            process_block(image[i_h:f_h, i_w:f_w])
            img_obj.new_image = image
            img_obj.write()
            print("============")
