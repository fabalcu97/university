from os.path import dirname, realpath

from cv2 import (
    imread,
    imdecode,
    imencode,
    imwrite,
    threshold,
    morphologyEx,
    MORPH_OPEN,
    THRESH_BINARY,
    THRESH_OTSU,
    IMREAD_GRAYSCALE,
    IMREAD_UNCHANGED,
    IMREAD_COLOR,
)
from numpy import ones, dot, zeros, array, median, pi, e, exp, empty
from constants import (
    SOBEL,
    COLOR_AMOUNT,
)


class ImageNotFound(Exception):
    pass


class ImageReader():

    image_path = f"{dirname(realpath(__file__))}/input/"

    def __init__(self, image_name, mask_dimensions=(3, 3), mode=IMREAD_GRAYSCALE, *args, **kwagrs):
        exception_str = "Image '{name}' not found".format(
            name=self.image_path + image_name)

        self.name = image_name.split('.')[0]
        self.extension = image_name.split('.')[1]
        self.image = imread(self.image_path + image_name, mode)
        self.mask_dimensions = mask_dimensions
        self.left_right = int((self.mask_dimensions[1] - 1) / 2)
        self.up_bottom = int((self.mask_dimensions[0] - 1) / 2)

        if self.image is None:
            raise ImageNotFound(exception_str)

        self.rows = self.image.shape[0]
        self.columns = self.image.shape[1]
        self.channels = self.image.shape[2] if len(
            self.image.shape) == 3 else 1
        self.total_pixel = self.rows * self.columns

        self.new_image = zeros((self.rows, self.columns, self.channels))

    def __getitem__(self, key):
        return self.image[key]

    def __setitem__(self, key, value):
        self.new_image[key] = value

    def __str__(self):
        return str(self.image)

    def set_binary(self, treshold=127):
        (thresh, self.binary_image) = threshold(
            self.image, treshold, 255, THRESH_BINARY)

    def write(self):
        imwrite(f"./output/{self.name}.{self.extension}", self.new_image)

    def get_histogram(self):
        if hasattr(self, "histogram") and self.histogram:
            return self.historgram
        self.histogram = [[] for i in range(COLOR_AMOUNT)]
        for i in range(self.rows):
            for j in range(self.columns):
                self.histogram[self.image[i, j]].append((i, j))
        return self.histogram

    def border(self, window, _filter=SOBEL):
        sum = 0
        for i in range(self.mask_dimensions[0]):
            sum += dot(_filter[i], window[i])
        return 0 if sum < 0 else sum

