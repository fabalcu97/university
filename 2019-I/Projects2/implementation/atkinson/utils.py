import re


def get_header(channel):
    return [f"{channel}_median", f"{channel}_deviation", f"{channel}_kurtosis", f"{channel}_theta",
            f"{channel}_low_alpha", f"{channel}_alpha", f"{channel}_beta", f"{channel}_gamma",
            f"{channel}_activity", f"{channel}_mobility", f"{channel}_complexity", f"{channel}_fractal"]


def get_columns(pd_dataset, columns=[]):
    return pd_dataset[['class'] + columns]
