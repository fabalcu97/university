
import pymrmr


def feature_selection(data, features=10):
    return pymrmr.mRMR(data, "MID", features)
