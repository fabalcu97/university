import numpy as np
from pandas import read_csv, DataFrame
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score

from feature_selection import feature_selection
from utils import get_columns
from cConstants import (
    EXPERIMENTS,
    CANDIDATES,
)

dimensions = ['arousal', 'valence']
classifiers = {
    'RBF.2': SVC(kernel='rbf', gamma=.2, decision_function_shape='ovo'),
    'RBF.05': SVC(kernel='rbf', gamma=.05, decision_function_shape='ovo'),
    'POLY': SVC(kernel='poly', gamma='auto', decision_function_shape='ovo', degree=5),
}
title = []
for classifier_key in classifiers.keys():
    title.append(f'{classifier_key}')

for dimension in dimensions:
    print(f'======== Dimension {dimension} ========')
    for i in range(10, 190, 10):
        print(f'======== {i} Features ========')
        tmp_results = [title]
        for user in range(CANDIDATES):
            print(f'======== User {user} ========')
            filename = f'./output/{dimension}_{user}.csv'
            main_dataset = read_csv(filename)
            features = feature_selection(main_dataset, features=i)
            dataset = get_columns(main_dataset, features)
            accuracies = list()
            for classifier_key in classifiers.keys():
                print(f'======== Classifier {classifier_key} ========')
                x = dataset.iloc[:, 1:].values
                y = np.ravel(dataset.iloc[:, :1].values)
                cf_validation = cross_val_score(
                    classifiers[classifier_key], x, y, cv=8)
                accuracies.append(np.average(cf_validation) * 100)
            tmp_results.append(accuracies)
        DataFrame(data=tmp_results).to_csv(
            f'./accuracy/{dimension}_{i}F.csv',
            header=None, index=None)
