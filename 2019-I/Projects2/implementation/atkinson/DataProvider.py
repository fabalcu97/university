from numpy import divide
from six.moves import cPickle as pickle

path = "/Users/fabalcu97/Downloads/DEAP/data_preprocessed_python/"


class DataProvider:
    def __init__(self, file_number, experiment=0):
        self.record = pickle.load(
            open(path + "s{n}.dat".format(n=file_number), 'rb'),
            encoding='latin1')
        self.labels = self.record['labels']
        self.data = self.record['data']
        self.experiment = experiment

    def set_experiment(self, experiment):
        """There are 40 experiments available. Each with 40 channels recorded."""
        self.experiment = experiment

    def get_matrix(self):
        """Get channels from 0 to 31."""
        return self.data[self.experiment][0:31]

    def get_channel(self, channel, normalized=True):
        """Get channel information, useful from 0 to 31. The difference, til 39, are different than eeg information."""
        data = self.data[self.experiment][channel]
        if normalized:
            return divide(data, max(data))
        return data

    def get_valence(self):
        """Valence experiment."""
        value = self.labels[self.experiment][0]
        if value <= 3.66:
            return -1
        elif 3.66 < value and value <= 6.33:
            return 0
        elif value > 6.33:
            return 1

    def get_arousal(self):
        """Arousal experiment."""
        value = self.labels[self.experiment][1]
        if value <= 3.66:
            return -1
        elif 3.66 < value and value <= 6.33:
            return 0
        elif value > 6.33:
            return 1
