from numpy import log2, power
from pywt import wavedec, dwt, Wavelet, waverec


def entropy(coefficients):
    s = 0
    for i in coefficients:
        tmp = power(i, 2)
        s += tmp * log2(tmp)
    return -s


def energy(coefficients):
    s = 0
    for coeff in coefficients:
        s += power(coeff, 2)
    return s


def get_features(window, level):
    _energy = list()
    _entropy = list()
    approx = list(window)
    for i in range(level):
        (approx, detail) = dwt(approx, 'db4')
        _energy.append(energy(detail))
        _entropy.append(entropy(detail))
    return _energy, _entropy
