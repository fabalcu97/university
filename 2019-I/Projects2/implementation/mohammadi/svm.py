import numpy as np
from pandas import read_csv
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC

from cConstants import CANDIDATES
from utils import get_columns

for exp_name in ['arousal', 'valence']:
    print(f"======{exp_name}=====")
    for user in range(CANDIDATES):
        filename = f'./output/{exp_name}_{user}.csv'
        dataset = read_csv(filename)
        for i in range(5):
            dataset = get_columns(dataset, band=i)
            x = dataset.iloc[:, :-1].values
            y = dataset.iloc[:, 100].values
            classifier = SVC(kernel='poly', C=200, gamma='auto')

            cf_validation = cross_val_score(classifier, x, y, cv=10)
            print(np.average(cf_validation) * 100)
