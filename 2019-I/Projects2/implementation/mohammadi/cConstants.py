FREQUENCIES = {
    'theta': (4, 8, ),
    'low_alpha': (8, 10, ),
    'alpha': (8, 12, ),
    'beta': (12, 30, ),
    'gamma': (30, 45, ),
}

CHANNELS_INDEX = {
    'f3': 2,
    'f4': 19,
    'f7': 3,
    'f8': 20,
    'fc1': 5,
    'fc2': 22,
    'fc5': 4,
    'fc6': 21,
    'fp1': 0,
    'fp2': 16,
}

SAMPLING = 128
CHANNELS = 32
CANDIDATES = 32
EXPERIMENTS = 40

TOTAL_FRAMES = 60
WINDOW_TIME_2 = 2
WINDOW_TIME_4 = 4

COLUMNS = ['eng_D1_f3', 'eng_D2_f3', 'eng_D3_f3', 'eng_D4_f3', 'eng_D5_f3',
           'ent_D1_f3', 'ent_D2_f3', 'ent_D3_f3', 'ent_D4_f3', 'ent_D5_f3',
           'eng_D1_f4', 'eng_D2_f4', 'eng_D3_f4', 'eng_D4_f4', 'eng_D5_f4',
           'ent_D1_f4', 'ent_D2_f4', 'ent_D3_f4', 'ent_D4_f4', 'ent_D5_f4',
           'eng_D1_f7', 'eng_D2_f7', 'eng_D3_f7', 'eng_D4_f7', 'eng_D5_f7',
           'ent_D1_f7', 'ent_D2_f7', 'ent_D3_f7', 'ent_D4_f7', 'ent_D5_f7',
           'eng_D1_f8', 'eng_D2_f8', 'eng_D3_f8', 'eng_D4_f8', 'eng_D5_f8',
           'ent_D1_f8', 'ent_D2_f8', 'ent_D3_f8', 'ent_D4_f8', 'ent_D5_f8',
           'eng_D1_fc1', 'eng_D2_fc1', 'eng_D3_fc1', 'eng_D4_fc1', 'eng_D5_fc1',
           'ent_D1_fc1', 'ent_D2_fc1', 'ent_D3_fc1', 'ent_D4_fc1', 'ent_D5_fc1',
           'eng_D1_fc2', 'eng_D2_fc2', 'eng_D3_fc2', 'eng_D4_fc2', 'eng_D5_fc2',
           'ent_D1_fc2', 'ent_D2_fc2', 'ent_D3_fc2', 'ent_D4_fc2', 'ent_D5_fc2',
           'eng_D1_fc5', 'eng_D2_fc5', 'eng_D3_fc5', 'eng_D4_fc5', 'eng_D5_fc5',
           'ent_D1_fc5', 'ent_D2_fc5', 'ent_D3_fc5', 'ent_D4_fc5', 'ent_D5_fc5',
           'eng_D1_fc6', 'eng_D2_fc6', 'eng_D3_fc6', 'eng_D4_fc6', 'eng_D5_fc6',
           'ent_D1_fc6', 'ent_D2_fc6', 'ent_D3_fc6', 'ent_D4_fc6', 'ent_D5_fc6',
           'eng_D1_fp1', 'eng_D2_fp1', 'eng_D3_fp1', 'eng_D4_fp1', 'eng_D5_fp1',
           'ent_D1_fp1', 'ent_D2_fp1', 'ent_D3_fp1', 'ent_D4_fp1', 'ent_D5_fp1',
           'eng_D1_fp2', 'eng_D2_fp2', 'eng_D3_fp2', 'eng_D4_fp2', 'eng_D5_fp2',
           'ent_D1_fp2', 'ent_D2_fp2', 'ent_D3_fp2', 'ent_D4_fp2', 'ent_D5_fp2']