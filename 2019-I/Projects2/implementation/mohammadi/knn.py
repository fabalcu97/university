import numpy as np
from pandas import read_csv
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsClassifier

from cConstants import CANDIDATES
from utils import get_columns

for exp_name in ['arousal', 'valence']:
    print(f"======{exp_name}=====")
    for user in range(CANDIDATES):
        filename = f'./output/{exp_name}_{user}.csv'
        dataset = read_csv(filename)
        x = dataset.iloc[:, :-1].values
        y = dataset.iloc[:, 100].values
        classifier = KNeighborsClassifier(n_neighbors=3)

        cf_validation = cross_val_score(classifier, x, y, cv=30)
        print(np.average(cf_validation) * 100)
