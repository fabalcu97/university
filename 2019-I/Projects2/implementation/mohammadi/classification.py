import numpy as np
from pandas import read_csv, DataFrame
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier

from cConstants import CANDIDATES
from utils import get_columns

classifiers = {
    'KNN': KNeighborsClassifier(n_neighbors=3),
    'SVM': SVC(kernel='poly', C=200, gamma='scale'),
}

# BANDS
# for exp_name in ['arousal', 'valence']:
#     print(f'========{exp_name}========')
#     for classifier_key in classifiers.keys():
#         print(f'========{classifier_key}========')
#         title = []
#         for i in range(1, 6):
#             title.append(f'D{i}_{classifier_key}')
#         tmp_results = [title]
#         for user in range(CANDIDATES):
#             print(f'========User {user}========')
#             filename = f'./output/{exp_name}_{user}.csv'
#             main_dataset = read_csv(filename)
#             accuracies = list()
#             for i in range(1, 6):
#                 print(f'========Band {i}========')
#                 dataset = get_columns(main_dataset, band=i)
#                 x = dataset.iloc[:, :-1].values
#                 y = dataset.iloc[:, len(dataset.columns) - 1].values
#                 cf_validation = cross_val_score(
#                     classifiers[classifier_key], x, y, cv=30)
#                 accuracies.append(np.average(cf_validation) * 100)
#             tmp_results.append(accuracies)
#         DataFrame(data=tmp_results).to_csv(
#             f'./accuracy/000_bands_{exp_name}_{classifier_key}.csv',
#             header=None, index=None)

# PAIRS
for exp_name in ['arousal', 'valence']:
    print(f'========{exp_name}========')
    for classifier_key in classifiers.keys():
        print(f'========{classifier_key}========')
        title = []
        for i in range(1, 6):
            title.append(f'{i}P_{classifier_key}')
        tmp_results = [title]
        for user in range(CANDIDATES):
            print(f'========User {user}========')
            filename = f'./output/{exp_name}_{user}.csv'
            main_dataset = read_csv(filename)
            accuracies = list()
            for i in range(1, 6):
                print(f'========Pair {i}========')
                dataset = get_columns(main_dataset, pairs=i)
                x = dataset.iloc[:, :-1].values
                y = dataset.iloc[:, len(dataset.columns) - 1].values
                cf_validation = cross_val_score(
                    classifiers[classifier_key], x, y, cv=30)
                accuracies.append(np.average(cf_validation) * 100)
            tmp_results.append(accuracies)
        DataFrame(data=tmp_results).to_csv(
            f'./accuracy/000_pairs_{exp_name}_{classifier_key}.csv',
            header=None, index=None)

# CHANNELS
for exp_name in ['arousal', 'valence']:
    print(f'========{exp_name}========')
    for classifier_key in classifiers.keys():
        print(f'========{classifier_key}========')
        title = []
        for i in range(1, 6):
            title.append(f'{i}C_{classifier_key}')
        tmp_results = [title]
        for user in range(CANDIDATES):
            print(f'========User {user}========')
            filename = f'./output/{exp_name}_{user}.csv'
            main_dataset = read_csv(filename)
            accuracies = list()
            for i in range(1, 6):
                print(f'========Channel {i}========')
                dataset = get_columns(main_dataset, channels=i)
                x = dataset.iloc[:, :-1].values
                y = dataset.iloc[:, len(dataset.columns) - 1].values
                cf_validation = cross_val_score(
                    classifiers[classifier_key], x, y, cv=30)
                accuracies.append(np.average(cf_validation) * 100)
            tmp_results.append(accuracies)
        DataFrame(data=tmp_results).to_csv(
            f'./accuracy/000_channels_{exp_name}_{classifier_key}.csv',
            header=None, index=None)

