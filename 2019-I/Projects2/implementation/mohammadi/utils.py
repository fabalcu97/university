import re
from cConstants import COLUMNS


def get_columns(pd_dataset, band=None, pairs=None, channels=None):
    _columns = COLUMNS
    if band:
        _columns = list(filter(lambda x: re.search(f'D{band}', x), COLUMNS))
    elif pairs:
        _columns = COLUMNS[0: pairs * 2 * 10]
    elif channels:
        _columns = COLUMNS[(channels * 20) - 20: channels * 20]
    return pd_dataset[_columns + ['class']]
