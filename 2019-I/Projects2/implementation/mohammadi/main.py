from pandas import DataFrame

from DataProvider import DataProvider
from feature_extraction import get_features
from cConstants import (
    CHANNELS_INDEX,
    SAMPLING,
    WINDOW_TIME_2,
    WINDOW_TIME_4,
    TOTAL_FRAMES,
    CANDIDATES,
    EXPERIMENTS,
    CHANNELS,
)

_time = WINDOW_TIME_4
window_size = int(SAMPLING * _time)


def get_time_windows(data):
    frames = []
    for i in range(0, TOTAL_FRAMES - _time + 1):
        lower = int(i * SAMPLING)
        upper = int((i * SAMPLING) + window_size - 1)
        if _time == 4 and i % 2 != 0:
            continue
        frames.append(data[lower: upper])
    return frames


def get_header(key):
    return [f'eng_D1_{key}', f'eng_D2_{key}', f'eng_D3_{key}', f'eng_D4_{key}',
            f'eng_D5_{key}', f'ent_D1_{key}', f'ent_D2_{key}', f'ent_D3_{key}',
            f'ent_D4_{key}', f'ent_D5_{key}']


level = 5
headers = []
for key, value in CHANNELS_INDEX.items():
    headers.extend(get_header(key))

for user in range(CANDIDATES):
    data_provider = DataProvider(user + 1)
    for dimension in ['arousal', 'valence']:
        features = dict()
        frequencies = list()
        count = 0
        for experiment in range(EXPERIMENTS):  # videos
            data_provider.set_experiment(experiment)
            _class = getattr(data_provider, f'get_{dimension}')()
            for key, value in CHANNELS_INDEX.items():
                features[key] = list()
                channel = data_provider.get_channel(value)
                windows = get_time_windows(channel)
                for idx, window in enumerate(windows):
                    eng, ent = get_features(window, level)
                    features[key].append(eng + ent)
                count = len(features[key])
            for i in range(count):
                tmp = []
                for key in features.keys():
                    tmp.extend(features[key][i])
                frequencies.append(tmp + [_class])
        frequencies.insert(0, headers + ['class'])

        df = DataFrame(data=frequencies)
        df.to_csv(f'./output/{dimension}_{user}.csv', header=None, index=None)

        del frequencies
        del features
        del count
    del data_provider
