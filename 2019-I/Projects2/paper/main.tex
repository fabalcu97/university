\documentclass[journal]{IEEEtran}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{float}
\usepackage{amsmath}
\usepackage{tabularx,booktabs}
\usepackage{svg}

\begin{document}

\title{Comparación de Métodos de Reconocimiento de Emociones Basados en un Modelo Bidimensional}
\author{\IEEEauthorblockN{Fabricio Ballón Cuadros}\\
\IEEEauthorblockA{fabricio.ballon@ucsp.edu.pe}}

\maketitle

\begin{abstract}
La computación afectiva nos impulsa a reconocer el estado emocional de una persona mediante un análisis no invasivo. Al ser las emociones tan variables entre los usuarios, nos encontramos con el problema de reconocer con la mayor precisión posible las mismas. Este artículo explicará el proceso que se sigue para el reconocimiento de emociones y presentará una comparación entre dos técnicas del estado del arte con el fin de determinar la mejor y, a su vez, presentar posibles trabajos futuros para mejorar dicho reconocimiento.
\end{abstract}

\section{Introducción}
Las emociones son sensaciones que los seres humanos perciben a causa de diferentes estímulos. Estas emociones tienen una fuerte influencia en el trabajo de una persona y su actuar cotidiano, por esta razón un buen manejo de las emociones puede llevar a un mejor desempeño de las acciones cotidianas\cite{Judge1998, Pervez2010}. Sin embargo, por la naturaleza de los estímulos, no siempre se puede contener las emociones y mantenerlas en un estado óptimo. En base a esto, determinar el estado emocional de una persona puede proveer información beneficiosa para la productividad en diferentes ámbitos, como los deportes\cite{Lazarus2000} o el trabajo\cite{Pervez2010}.
\\
El reconocimiento de emociones se halla entre los objetivos de la computación afectiva\cite{Mohammadpour2017, Banafa2016}, una rama de la computación que además busca interpretar, procesar y simular la afectividad humana. Entre los métodos para lograr reconocer emociones se encuentra visión computacional o procesamiento de voz, sin embargo estos tienen distintos requerimientos, como múltiples cámaras o que el usuario este hablando constantemente.
\\
Gracias a las \begin{it}Brain-Computer interfaces\end{it} (BCI), el reconocimiento de emociones es posible por medio de análisis en las señales extraídas por electroencefalogramas (EEG). Existen múltiples métodos y así también distintas aplicaciones, por ejemplo en el caso de los videojuegos, en base a las emociones del jugador, se puede modificar el entorno para lograr una experiencia de juego más envolvente.
\\
El objetivo de este documento es presentar los resultados de la implementación de dos técnicas del estado del arte, para hacer comparaciones y determinar, en base a los resultados, la mejor de estas.
\\
El resto de este trabajo está estructurado de la siguiente manera, la sección II indicará el flujo de extracción hasta la etapa de clasificación, la sección III explicará el estado del arte. La sección IV será la explicación del experimento, la sección V describirá los resultados de las pruebas, la sección VI indicará las conclusiones y el trabajo futuro.
\\

\section{Adquisición y preprocesamiento}
El procesamiento de señales cerebrales consta de varias etapas. La extracción\cite{Major2014} puede tomar diferentes formas; puede ser invasiva, es decir, se requiere de una cirugía y por lo mismo tienen mejor resolución espacial. También pueden ser no invasivas\cite{Waldert2016}, la más común y rentable es la extracción por electroencefalograma (EEG), que consta de un arreglo de electrodos sencillo y lectura de señales electromagnéticas. Tiene buena resolución temporal pero no espacial. El posicionamiento de los electrodos viene dictado por un estándar internacional denominado sistema internacional para el posicionamiento de electrodos 10-20. El arreglo se da como se puede ver en la Figura \ref{fig:10_20}.
\\
\begin{figure}[H]
  \centering
  \includegraphics[height=7cm]{assets/10-20.png}
  \caption{Sistema de posicionamiento de electrodos 10-20}
  \label{fig:10_20}
\end{figure}
El preprocesamiento se encarga de eliminar el ruido o señales que no son de importancia al resultado final, se consideran los latidos del corazón, la respiración o el movimiento de los ojos\cite{Tandle2015}. Para eliminar este ruido se usan filtros\cite{Vaid2015} como \textit{High-pass} (HP), \textit{Low-Pass} (LP) o \textit{Notch} que eliminan ciertas frecuencias, dejando así solo un rango de señales con información valiosa para el procesamiento. La selección de características permite escoger qué atributos de las señales adquiridas serán utilizados para el reconocimiento de las emociones, es usual que se descomponga las señales hasta llegar a los rangos especificados por cada tipo, \textit{delta}, \textit{theta}, \textit{alpha}, \textit{beta} y \textit{gamma}. Finalmente la clasificación toma en consideración la información de los pasos previos para poder determinar la clase de la señal que alimenta el sistema. El flujo se puede visualizar en la Figura \ref{fig:process} y se da de manera transversal en el reconocimiento de patrones en la actividad cerebral, sean emociones, actividades motoras o cualquier otra actividad cerebral que se busque determinar.
\begin{figure}[H]
  \centering
  \includegraphics[height=6cm]{assets/process.jpg}
  \caption{Descripción del proceso llevado a cabo en BCI\cite{Vaid2015}}
  \label{fig:process}
\end{figure}

\section{Estado del arte}
El estado del arte muestra diferentes métodos para el reconocimiento de emociones. Heger et al. propone una clasificación en dos etapas basadas en \begin{it}Support Vector Machine (SVM)\end{it} para reconocer un conjunto de emociones básicas y también expresiones faciales. Finalmente la clasificación obtiene una precisión de 81.8\%\cite{Heger2011}.
Para estimular las emociones del usuario, Jalilifard (2016)\cite{Jalilifard2016} muestra vídeos en determinadas ventanas de tiempo, se extraen las características mediante \begin{it}Stationary Wavelet Transform\end{it} (SWT), aprovechando su pequeña variación ante cambios pequeños y finalmente se realiza la clasificación con SVM. Otro enfoque\cite{Mohammadi2016} parte de la selección de 10 canales de información en base a la actividad cerebral y la ubicación física de la misma, se filtra el ruido, se extraen características haciendo uso de DWT y finalmente se realiza la clasificación con SVM y \begin{it}K-Nearest Neighbour\end{it} (KNN), generando resultados con 86\% de precisión.
\\
Se presenta un método de reconocimiento\cite{Atkinson2016} que introduce la selección de características en el proceso de clasificación y aumenta la cantidad de emociones a reconocer haciendo uso de un SVM de múltiple clases. El proceso se realiza grabando la actividad cerebral, haciendo un seguimiento ocular, para así poder eliminar este ruido, una vez hecho esto, se filtra la información generando bandas entre 4Hz y 45Hz, pues ahí se encuentra la información más útil para la clasificación de emociones. Para la extracción y selección de características se elimina los elementos redundantes por medio de \begin{it}minimum-Redundancy-Maximum-Relevance\end{it} (mRMR) y se selecciona los elementos mejor relacionados a la variable de clasificación haciendo uso de \begin{it}Mutual Information\end{it} (MI), finalmente se realiza una clasificación por medio de un SVM. Los resultados muestran una precisión del 60\%, considerando múltiples clases en una clasificación.
\\
Una extracción basada en \begin{it}Discrete Wavelet Transform\end{it} (DWT), considerando tres características particulares a partir de las señales, ha logrado realizar una clasificación adecuada por medio de \begin{it}Fuzzy C-Mean\end{it} (FCM), sin embargo es necesaria mucha información de extracción, múltiples canales de EEG\cite{Murugappan2008}.  
Otro autor\cite{Mohammadpour2017} propone hacer una clasificación en base a un plano en dos dimensiones, \begin{it}Arousal-Valence\end{it}, que busca mapear las emociones en base a la intensidad y su percepción (positiva o negativa), respectivamente. Realizó la extracción de características por medio de DWT, redujo los vectores de características haciendo uso de \begin{it}Principal Component Analysis\end{it} (PCA) y finalmente clasifica los vectores con una \begin{it}Artificial Neural Network\end{it} (ANN) obteniendo resultados del 55.58\%, posicionándose por encima de otros algoritmos del estado del arte.
\\
Otra propuesta\cite{Lee2014} realiza un análisis con un enfoque diferente, en lugar de considerar únicamente determinada actividad cerebral, hacen un análisis que enlaza diferentes aspectos de la misma, toman en consideración la actividad de diferentes áreas del cerebro, adquiriendo una mayor cantidad de información. Con la información grabada, se determinan tres indicadores: la correlación que indica la relación entre dos señales, la coherencia que es similar a la correlación pero agrega la covarianza en función de la frecuencia y la sincronización de fase de las señales que estima la diferencia entre la fase de las señales. Siendo el punto fuerte de su propuesta determinar las tres características indicadas y realizar la clasificación en base a estas. La selección de características significativas se realiza por medio de \begin{it}ANOVA\end{it} para llegar a la clasificación por medio de \begin{it}Quadratic Discriminant Analysis\end{it} (QDA). Los resultados muestran que cada uno de los indicadores provee mejor precisión al momento de clasificar, sin embargo la que más resalta es la sincronización de fase.

\section{Marco Teórico}
La clasificación de emociones se compone de varios conceptos, primero se explicará las señales involucradas, seguido del tipo de reconocimiento de emociones. Luego se explicarán las distintas etapas, los métodos involucrados y su aplicación en las técnicas a comparar.
\subsection{Tipos de señales}
Las ondas correspondientes a las señales que se extraen tienen diferentes frecuencias y se asocian a diferentes actividades.\cite{Ramadan2015, Kanaga2017}
\subsubsection{Ondas Delta}
 Estas ondas se hallan en el rango de 0.5 a 4 Hz como se ve en la figura \ref{fig:delta}. Son ondas lentas de gran amplitud, asociadas al sueño profundo y estado de vigilia.
 \begin{figure}[H]
  \centering
  \includegraphics[height=2cm]{assets/Delta_wave.png}
  \caption{Muestra de 1 segundo de una señal del tipo Delta}
  \label{fig:delta}
\end{figure}
 \subsubsection{Ondas Theta}
 Se hallan en el rango de 4 a 8 Hz, con un voltaje mayor a los 20 $\mu$V como se ve en la figura \ref{fig:theta}. Surgen ante estrés emocional, frustración o decepción, siendo así asociadas a la ineficiencia. También se relacionan con estados de creatividad, meditación o el soñar despierto.
 \begin{figure}[H]
  \centering
  \includegraphics[height=2cm]{assets/Theta_wave.png}
  \caption{Muestra de 1 segundo de una señal del tipo Theta}
  \label{fig:theta}
\end{figure}
 \subsubsection{Ondas Alpha}
 Se hallan en rangos entre 8 y 13 Hz con un voltaje entre de 30 a 50 $\mu$V como se ve en la figura \ref{fig:alpha}. Se relacionan con estados de ansiedad, concentración o al mantener la mente en blanco.
 \begin{figure}[H]
  \centering
  \includegraphics[height=2cm]{assets/Alpha_wave.png}
  \caption{Muestra de 1 segundo de una señal del tipo Alpha}
  \label{fig:alpha}
\end{figure}
\subsubsection{Ondas Beta}
 Se encuentran en rangos de 13 a 30 Hz, con voltajes entre 5 a 30 $\mu$V como se ve en la figura \ref{fig:beta}. Se asocian con el pensamiento activo, atención, concentración y resolución de problemas específicos. Durante una actividad mental intensa, pueden alcanzar los 50Hz.
\begin{figure}[H]
  \centering
  \includegraphics[height=2cm]{assets/Beta_wave.png}
  \caption{Muestra de 1 segundo de una señal del tipo Beta}
  \label{fig:beta}
\end{figure}
\subsubsection{Ondas Gamma}
 Se encuentran en rangos de  30 Hz en adelante como se ve en la figura \ref{fig:gamma}. Reflejan el mecanismo de conciencia de la persona.
\begin{figure}[H]
  \centering
  \includegraphics[height=2cm]{assets/Gamma_wave.png}
  \caption{Muestra de 1 segundo de una señal del tipo Gamma}
  \label{fig:gamma}
\end{figure}
\subsection{Reconocimiento}
El mayor problema para poder reconocer las emociones es que son subjetivas, por lo tanto se definen dos modelos.
\subsubsection{Modelo discreto} Consiste en la clasificación de emociones por medio de un reconocimiento básico como: miedo, alegría, tristeza, disgusto, sorpresa, entre otros.
\subsubsection{Modelo bidimensional} Este modelo nos permite clasificar las emociones basándose en el nivel de excitación, calma o excitación, y la valencia, disgusto o placer, generando un plano bidimensional simple y generalizado como se puede ver en la figura \ref{fig:bidimensional}.
\begin{figure}[H]
  \centering
  \includegraphics[height=8cm]{assets/arousal_valence.png}
  \caption{Plano bidimensional de la excitación y la valencia}
  \label{fig:bidimensional}
\end{figure}
\subsection{Adquisición}
La adquisición consiste en obtener la información de la actividad cerebral. Existen métodos invasivos, que requieren de cirugía y el posicionamiento de una malla de electrodos o un único electrodo. Estos proveen mejor información sobre las señales cerebrales, pero acarrean el riesgo de la cirugía.
Los métodos no invasivos son menos costoso, seguro, sin embargo la información que obtienen es débil en comparación con los métodos no invasivos a causa del cráneo. Consisten en un juego de electrodos posicionados en el escalpelo y la detección más usada es por medio de electroencefalogramas (EEG)\cite{Ramadan2015}.
\subsection{Preprocesamiento}
En el proceso de adquisición, las señales extraídas resultan con interferencia y ruidos que pueden afectar las siguientes etapas. El preprocesamiento consiste en eliminar aquellos \begin{it}artifacts\end{it} o errores de observación que no son útiles para la detección de emociones.
Estos pueden ir de los ruidos generados por los equipos electrónicos, corriente eléctrica\cite{Mohammadi2016}, así como ruido generado por el movimiento involuntario de los ojos, los latidos del corazón, parpadeo, respiración, entre otros ruidos\cite{Tandle2015}.
\subsubsection{Reducción de resolución} Se puede eliminar las frecuencias mayores a los 128 Hz, esto porque la información útil para el reconocimiento de emociones se halla por debajo de los 40 Hz\cite{Liu2010}.
\subsubsection{Eliminación de ruido ocular} Se basa en la detección de las señales producidas por movimientos oculares mediante una electrooculografía. Consiste en descomponer la señal EEG haciendo uso de \begin{it}wavelets\end{it}, así se detectan las zonas donde hay movimiento o parpadeo, se eliminan y se recompone la señal EEG, descartando así el ruido.\cite{Xiaobai2016}
\subsubsection{Filtro de banda} Consiste en filtrar las señales en rangos entre 4 y 45 Hz, que es donde se ubica la información relevante para el reconocimiento de emociones.

\subsection{Selección de características}
    Consiste en obtener datos de las señales registradas para luego extraer los más relevantes.
    \subsubsection{Poder de banda} Indica la cantidad de información que provee una banda a la señal total. Para determinar el poder de una banda es necesario convertir la señal del dominio de tiempo al dominio de frecuencia, esto se logra mediante la transformada de Fourier\cite{Schwilden2006, Vallat2018}. Con la señal expresada en función de la frecuencia, se determina el rango de la señal a extraer y se halla el área debajo de dicha señal, siendo resultado de dicha área el poder.
    \subsubsection{Parámetros de Hjorth} Son propiedades estadísticas de una señal\cite{Oh2014, Hjorth1970}. Consta de tres tipos de parámetros:
        \begin{itemize}
            \item Actividad es la media de la varianza de la amplitud respecto al tiempo y representa el poder de la señal.
                \begin{equation}
                \label{eqn:activity}
                    A = var(y(t))
                \end{equation}
            \item Movilidad es una estimación de la frecuencia media.
                \begin{equation}
                \label{eqn:mobility}
                    M = \sqrt{\frac{var(\frac{dy(t)}{dt})}{var(y(t))}}
                \end{equation}
            \item Complejidad indica el cambio en la frecuencia.
                \begin{equation}
                \label{eqn:complexity}
                    C = \frac{M\frac{dy(t)}{dt}}{M(y(t))}
                \end{equation}
        \end{itemize}
    \subsubsection{Características estadísticas} Un análisis estadístico básico de las señales, son fuente de información significativa\cite{Yuen2013}. $N$ representa la cantidad de muestras tomadas, y $X_{n}$ el valor de la $n$-ésima muestra. Se considera la señal pura para las siguientes operaciones.
        \begin{itemize}
            \item Media
                \begin{equation}
                    \label{eqn:mean}
                    \mu_{x} = \frac{1}{N}\sum_{n=1}^{N}X_{n}
                \end{equation}
            \item Desviación estándar
                \begin{equation}
                    \label{eqn:deviation}
                    \sigma_{x} = (\frac{1}{N-1}\sum_{n=1}^{N}(X_{n} - \mu_{x})^{2})^{\frac{1}{2}}
                \end{equation}
            \item Media del valor absoluto de la primera diferencia
                \begin{equation}
                    \label{eqn:first_difference}
                    \delta_{x} = \frac{1}{N-1}\sum_{n=1}^{N-1}\mid N_{n+1} - X_{n}\mid
                \end{equation}
            \item Media del valor absoluto de la primera diferencia de la señal normalizada
                \begin{equation}
                    \label{eqn:normalized_first_difference}
                    \tilde{\delta_{x}} = \frac{\delta_{x}}{\sigma_{x}}
                \end{equation}
        \end{itemize}

\subsection{Extracción de características}
    De la selección de características, se debe extraer las más relevantes para el procesamiento. Existen diferentes enfoques.
    \subsubsection{Mínima redundancia, máxima relevancia (mRMR)} Selecciona las características que tienen una fuerte correlación con la variable de clasificación, descartando así información redundante y no útil. La mínima redundancia se define como:
    \begin{equation}
        \label{eqn:min_redundancy}
        min W_{I}, W_{I} = \frac{1}{\mid S \mid^{2}}\sum_{f_{i}, f_{j} \in S} I(f_{i}, f_{j})
    \end{equation}
    Donde $I(f_{i}, f_{j})$ es la información mutua entre las características $f_{i}$ y $f_{j}$, $\mid S \mid = n$ es el número de características del conjunto.
    La máxima relevancia se determina como $I(C, f_{i})$, siendo la relación entre de la característica hacia la clase. La fórmula es:
    \begin{equation}
        \label{eqn:max_relevance}
        max V_{I}, V_{I} = \frac{1}{\mid S \mid}\sum_{f_{i} \in S} I(C, f_{j})
    \end{equation}
    \subsubsection{Transformada discreta de Wavelets} Consiste en realizar una descomposición de la señal. Para ello se considera una \begin{it}wavelet\end{it} madre y se relaciona con la señal a descomponer. Se aplican filtros HP, para obtener coeficientes detallados y LP para obtener los coeficientes de aproximación. Una vez realizado el filtrado, se disminuye el muestreo de la señal que pasó por LP en la mitad. Se realiza el mismo proceso hasta llegar al nivel de descomposición esperado\cite{Mohammadi2016, Cheong2015}. De las frecuencias descompuestas se puede obtener información como:
    \begin{itemize}
        \item Entropia es la aleatoreidad en la señal.
            \begin{equation}
                \label{eqn:entropy}
                ENT_{j} = - \sum_{k=1}^{N}(D_{j}(k)^{2})log(D_{j}(k)^{2})
            \end{equation}
        \item Energía o poder.
            \begin{equation}
                \label{eqn:energy}
                ENG_{j} = \sum_{k=1}^{N}(D_{j}(k)^{2})
            \end{equation}
        Donde $j$ es el nivel de la descomposición \begin{it}wavelet\end{it} y $k$ es el número del coeficiente \begin{it}wavelet\end{it}.
    \end{itemize}
    
\subsection{Clasificación}
Es la etapa final del proceso y busca reconoces las emociones a partir de la información obtenida previamente.
    \subsubsection{Support Vector Machine} Es un algoritmo basado en teoría de aprendizaje estadístico, mapea patrones de entrada en espacios de mayores dimensiones, basándose en las características de las entradas. Esto se logra por medio de una función \begin{it}kernel\end{it}, que se encarga de traducir al espacio de múltiples características\cite{Li2014, Liu2014}.
    \begin{itemize}
        \item \textit{Kernel} polinomial (PolyKernel): Permite la clasificación de modelos no lineales. $x_{i}$ y $x_{j}$ son los vectores a evaluar, $C$ es una constante que permite cambiar la influencia de los elementos de mayor o menor orden y $d$ es el grado del polinomio. Se da por la Ecuación \ref{eqn:polykernel}.
        \begin{equation}
            \label{eqn:polykernel}
            k(x_{i}, x_{j}) = (x_{i}\cdotp x_{j} + C)^{d}
        \end{equation}
        \item \textit{Radial Basis Kernel} (RBF): Se da por la Ecuación \ref{eqn:rbf}, y $\sigma$ suaviza la influencia de cada punto en los vectores.
        \begin{equation}
            \label{eqn:rbf}
            k(x_{i}, x_{j}) = exp(-\frac{\| \mathbf{x_{i} - x_{j}} \|^{2}}{2\sigma^{2}})
        \end{equation}
    \end{itemize}
    \subsubsection{K-Nearest Neighbours (KNN)} Consiste en clasificar información desconocida mediante una comparación con las clases definidas. 
    \begin{itemize}
        \item Recibe información sin clasificar.
        \item Realiza una medición de distancia, como euclidiana, contra los datos ya clasificados.
        \item Obtiene las $K$ menores distancias.
        \item Cuenta la cantidad de repeticiones de las clases con las distancias $K$ y elige la clase con más repeticiones.
        \item Continúa la clasificación considerando el nuevo valor.
    \end{itemize}


\section{Técnicas a implementar}
    \subsection{Sistema de reconocimiento de emociones basado en wavelets usando señales EEG}
    La propuesta de los autores Zeynab Mohammadi, Javad Frounchi1 y  Mahmood Amiri, realizada en 2016\cite{Mohammadi2016}, presenta un sistema de reconocimiento de emociones a partir de señales EEG restringidas en ventanas temporales, basándose en características de las señales y clasificando la información por medio de SVM y KNN sobre un modelo bidimensional, basado en la excitación y la valencia.
    \subsubsection{Procedimiento}
    La información se obtiene de una base de datos para análisis de emociones usando señales fisiológicas\cite{Koelstra2012}, DEAP por sus siglas en inglés. Se compone de información de 32 individuos, a los cuales les fueron mostrados 40 vídeos musicales.
    Estos vídeos fueron seleccionados cuidadosamente con el fin de proporcionar valores en la excitación y valencia lo suficientemente amplios.
    Por cada individuo se registró la siguiente información:
    \begin{itemize}
        \item Electroencefalografía (EEG), que es el registro de las señales electricas cerebrales.
        \item Electromiografía (EMG), que es el registtro de los impulsos eléctricos musculares.
        \item Electrooculografía (EOG), que es el registro del movimiento ocular.
        \item Temperatura de la piel
        \item Patrón de respiración
        \item Presión de la sangre
        \item Respuesta galvánica de la piel (GSR), que es la medida de las variaciones en las características eléctricas de la piel.
    \end{itemize}
    Luego se realiza una selección de canales de información relevantes para el estudio. Las emociones positivas se relacionan con la actividad frontal izquierda, mientras que las emociones negativas, con la actividad frontal derecha del cerebro. Por ello se eligen los pares de canales F3-F4, F7-F8, FC1-FC2, FC5-FC6 y FP1-FP2, pues cubren las áreas especificadas.
    \\
    El preprocesamiento se realiza obteniendo la media de ruidos intensos, como la corriente, amplificadores o interferencias externas, una vez obtenidos, se eliminan de la señal usando filtros HP, LP y \textit{Notch}. Finalmente se normalizan los valores de la señales para recudir la complejidad computacional, se normalizan en el intervalo [0, 1].
    \\
    Para la extracción de características se descompone la señal haciendo uso de DWT con una \textit{wavelet} madre $db4$, se selecciona porque es más apropiada para procesar señales biomédicas\cite{Cheong2015}. Se toma en consideración porciones de la señal de 2 a 4 segundos de longitud y se descomponen en 5 niveles. Seguidamente se extraen dos características en cada uno de los niveles de las bandas, la entropía y la energía, haciendo uso de las Ecuaciones \ref{eqn:entropy} y \ref{eqn:energy} respectivamente.
    \\
    La clasificación se realiza usando SVM y KNN, con una validación repetida 10 veces. Los pasos de dicha validación son:
    \begin{enumerate}
        \item Se divide el número de muestras en 10 conjuntos separados.
        \item Se usan 9 conjuntos para el entrenamiento y uno para pruebas.
        \item Se repite el paso 2, 10 veces, permutando la información en cada repetición.
    \end{enumerate}
    Para la clasificación mediante SVM se utiliza un \textit{kernel} polinomial, con un valor para la constance $C = 200$. Para la clasificación mediante KNN, se consideraron valores para $K$ entre 2 y 5, obteniendo la mejor clasificación con $K=3$.
    \\
    Los resultados muestran que de todos los canales registrados, el que mejor precisión obtuvo fue el par FP1-FP2, con un 74.6\% para la excitación y 80.68\% para la valencia usando KNN. El trabajo menciona que a raíz de esto se puede descartar el resto de canales para reducir el tiempo y costo computacional. Respecto a la información en las bandas, se determinó que la mayor precisión se halló en la banda \textit{gamma}, con un 80.34\% para la excitación y un 84.82\% para la valencia usando KNN. Finalmente se realizó el proceso tomando en consideración diferentes cantidades de pares de canales, llegando a la conclusión que, a mayor cantidad de pares de canales, mayor será la precisión en la clasificación. Con el uso de 5 pares de canales se logró una precisión de 84.05\% en excitación y 86.75\% en valencia usando KNN.
    \\
    El estudio concluye que KNN logra mejores resultados que SVM bajo las especificaciones señaladas previamente, además de lograr mayor precisión que los algoritmos que se hallan en el estado del arte. Como trabajos futuros proponen introducir nuevas características en el análisis de las señales, como la introducción de características caóticas o el análisis de Poincaré, además de la implementación de un sistema de \textit{hardware} para el reconocimiento de emociones.
    
    \subsection{Mejoramiento del reconocimiento de emociones basado en BCI combinando selección de características EEG y clasificadores kernel}
    John Atkinson y Daniel Campos en 2016\cite{Atkinson2016}, realizan una clasificación que realiza una selección de características basada en la cantidad de información correlacionada y no redundante. Con esta información entrenan tres SVMs y un sistema de votación para clasificar las emociones en un modelo bidimensional, basado en excitación y valencia. La información se extrae del conjunto de datos DEAP.
    \\
    El preprocesamiento incluye la reducción de la resolución de las señales, eliminando toda señal por encima de los 128Hz. La elimincación de los \textit{artifacts} oculares. Gracias a que el conjunto de datos incluye información sobre EOG, se puede eliminar dicho ruido de las señales. Finalmente, ya que las bandas usadas para la detección de emociones oscila entre los 4 y 45 Hz, se elimina el resto de frecuencias, dejando solo la sección útil.
    \\
    La selección y extracción de características se realiza usando diferentes métricas:
    \begin{itemize}
        \item Características estadísticas como la media en la Ecuación \ref{eqn:mean}, desviación estándar en la Ecuación \ref{eqn:deviation}, Media del valor absoluto de la primera diferencia en la Ecuación \ref{eqn:first_difference} y Media del valor absoluto de la primera diferencia de la señal normalizada en la Ecuación \ref{eqn:normalized_first_difference}.
        \item Poder de la banda, que registra la cantidad de energía que aporta una banda a la señal total.
        \item Parámteros de Hjorth, que son la actividad en la Ecuación \ref{eqn:activity}, la movilidad en la Ecuación \ref{eqn:mobility} y la complejidad en la Ecuación \ref{eqn:complexity}.
    \end{itemize}
    Una vez que se determinan dichas características, se realiza la selección. Primero se reduce la información redundante, eliminando dichas características, esto se logra haciendo uso de \ref{eqn:min_redundancy}. Una vez que se obtienen los datos no redundantes, se realiza una evaluación sobre la correlación de estos mediante la Ecuación \ref{eqn:max_relevance}. El resultado de los datos con la máxima relevancia se traduce en valores discretos mediante
    \begin{equation}
        \label{eqn:cases}
        f(x) = \begin{cases}
                    1,&  \textit{if } x \geq \mu + \frac{\sigma}{2}\\
                    0,&  \textit{if } \mu - \frac{\sigma}{2} \leq x \less \mu + \frac{\sigma}{2}\\
                    -1,& \textit{if } x \less \mu - \frac{\sigma}{2}\\
            \end{cases}
    \end{equation}
    Donde $\mu$ es la media de el valor de cada característica y $\sigma = 0.5$.
    \\
    Para la clasificación de diferentes clases mediante SVM se entrenó un conjunto de características $$E = {x_{i}, y_{i}}_{i=1}^{N}$$, donde N es el número de muestras de las características, $x_{i}$ es el vector de características a evaluar y $y_{i}$ es la dimensión de la clase. Se entrenaron 3 SVMs para la clasificación con \textit{kernel} RBF, determinado por la Ecuación \ref{eqn:rbf} y el resultado de los tres pasa por un proceso de votación, que determina cuál resultado es más preciso respecto a la clase y se determina un ganador.
    \\
    Las pruebas se dieron con diferentes parámetros, en el caso del RBF, se determinó un valor para $$-\frac{1}{2\sigma^{2}}$$ de $0.2$ y de $0.05$. Se usó como pureba un \textit{kernel} polinomial, al cual se le asignó un valor en el grado de $5$. Los resultados muestran que el kernel RBF en ambos casos superó al polinomial, con una precisión de casi 60\% contra un 50\%. Así también, se determinó que el valor óptimo para la longitud de características se ubica entre 29 y 35, causando pérdida en la precisión cuando esta longitud aumenta. Se realizaron también comparaciones contra un método que clasifica en base a algoritmos genéticos y SVM (GA-SVM), este método demostró ser más constante al momento de aumentar la longitud de características, sin embargo su precisión se mantuvo por debajo del 57\%. El método propuesto con selección de características mediante mRMR logró una precisión del 60.72\% en excitación y 62.33\% en valencia.
    \\
    La investigación concluye que el uso de mRMR mejora la precisión en SVM basado en un modelo bidimensional. Como trabajos futuros proponen realizar una selección de características basada en más parámetros que se puede obetener de las señales cerebrales. Proponen a su vez conseguir o generar un conjunto de datos específicos para el entrenamiento y así evitar información no útil para las investigaciones. Finalmente buscarán incrementar el número de clases que los SVM pueda clasificar.

\subsection{Conclusiones}
La investigación respecto a los algoritmos y el marco teórico deja abiertas varias posibilidades de mejora en las etapas de selección de características y clasificación. Ambos algoritmos usan SVM para realizar la clasificación con distintos enfoques, uno realiza la clasificación para múltiples clases y otro para realizar una comparación contra KNN, sin embargo, la función kernel puede ser modificada y mejorar el rendimiento. El primer método puede obtener aún más características de las señales descompuestas, alimentando así con mayor precisión a los clasificadores, sin embargo, la selección de pares de canales de información es una innovación que en los trabajos incluidos en el estado del arte no se ve y evidentemente mejora la precisión con la poca información que se extrae.

\bibliography{bibliography}{}
\bibliographystyle{plain}
\end{document}
